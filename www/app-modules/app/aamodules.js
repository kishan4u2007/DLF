if (typeof(Storage) != "undefined") {
    // Store local storage serverName
    localStorage.setItem("dataServerName", "http://discern.pro-digi.co.in/gitdiscern/");

    if (localStorage.getItem("functionTracking") === null) {
        // Local storage for creating Record to user events
        var d = new Date();
        var trackingElement = [{'functionName': 'AppStart','timeStamp': d}] ;
        localStorage.setItem("functionTracking", JSON.stringify( trackingElement));
    }

} else {
    alert('Error Occured While Connecting');
}

// angular.module('starter.controllers', ['starter.services', 'ngOpenFB'])
// 


document.addEventListener('deviceready', function() { 
    angular.bootstrap(document, ['starter']);
}, false);




var appModule = angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'ionicLazyLoad', 'ngOpenFB', 'ngTouch','nlFramework','ngPhotoswipe', 'ui-rangeSlider','ngSanitize', 'ng-walkthrough']);

var appModuleController = angular.module('starter.controllers', ['ngOpenFB','ngSanitize', 'ionic.cloud']);
 
 