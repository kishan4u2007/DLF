/**
 * Created by rahul.meghlan on 9/14/2015.
 */
var BASE_URL = "http://discern.pro-digi.co.in",
  BASE_URL_NEW = "http://discern.pro-digi.co.in/gitdiscern/",
  LIVE_URL = "http://service.discernliving.com/discern/", // old http://54.175.133.187/
  STAGING_URL = "http://staging.discernliving.com/discern/",

  LIVE_URL = STAGING_URL,

  QA_URL = "http://discernqa.pro-digi.co.in/",
  LOCAL_URL = "http://10.20.1.121",
  BLOG_URL = "http://www.discernliving.com/",
  imgPath = LIVE_URL + 'sites/default/files/',
  shareDisURL = 'getinfo/save_quiz_result';


  var marketUrl = {
    native:{
      iOS:'itms-apps://itunes.apple.com/us/app/angry-birds/id1191273950?mt=8&uo=4',
      android:'market://details?id=com.discernliving.app'
    },
    web:{
      android: 'https://play.google.com/store/apps/details?id=com.discernliving.app&hl=en'
    },
    getinfo:{
      iOS: 'https://itunes.apple.com/lookup?bundleId=in.discernliving.discern'
    }
  };

var getSVG = function () {
  var svg = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="32px" height="35px" viewBox="0 0 177.99 195" enable-background="new 0 0 177.99 195" xml:space="preserve">';
  svg += '<g id="discern-svg-loader" >';
  svg += '<path id="rotator" fill="#E64E38" d="M0.391,195.139c0-65.074,0-129.663,0-194.776c1.631,0,3.075,0,4.52,0c36.818,0,73.637,0.042,110.456-0.078   c3.2-0.011,5.057,0.851,6.719,3.672c17.722,30.082,35.571,60.086,53.427,90.088c1.416,2.377,2.353,4.268,0.584,7.21   c-18.064,30.068-35.949,60.246-53.856,90.408c-1.304,2.192-2.421,3.791-5.626,3.776c-37.984-0.164-75.969-0.106-113.954-0.116   C2.019,195.324,1.379,195.223,0.391,195.139z M17.641,178.168c1.033,0.133,1.826,0.324,2.62,0.324   c28.799,0.016,57.599,0.066,86.397-0.121c1.647-0.01,3.907-1.604,4.825-3.105c10.565-17.293,20.945-34.701,31.33-52.104   c3.188-5.346,6.24-10.773,9.453-16.338c-45.279,0-89.929,0-134.625,0C17.641,130.828,17.641,154.398,17.641,178.168z M17.628,89.17   c44.889,0,89.518,0,134.633,0c-0.759-1.344-1.221-2.202-1.718-3.039c-12.922-21.726-25.888-43.427-38.719-65.205   c-1.529-2.596-3.205-3.614-6.286-3.6c-27.831,0.128-55.663,0.071-83.495,0.09c-1.43,0.001-2.86,0.206-4.415,0.325   C17.628,41.751,17.628,65.34,17.628,89.17z"/>';
  svg += '</g>';
  return svg;
};
var getLoader = function () {
  var loader = '<span id="discern-svg-loader"><img id="rotator" src="img/logo_still.png"/></span>';
  return loader;
};

var UAT = LIVE_URL == STAGING_URL;

var globle_cache = true;
var login_urls = {
  user_persona: "user_quiz_result/get_persona/persona_result",
  user_login: "login_out/user/login",
  //user_logout: "applogout/user/logout"
  user_logout: "login_out/user/logout"
};

var majorMinor = {
  35: 32,
  48: 32,
  38: 32,
  231: 32,
  40: 33,
  43: 33,
  44: 33,
  41: 33,
  45: 34,
  46: 34,
  49: 34,
  47: 34,
  232: 230,
  37: 230,
  39: 230,
  36: 230
};

var getShareUrl = function (itemId, pageType) {

  var prefix = LIVE_URL + "share.html?";

  if (itemId && pageType) {

    if (pageType == "look") {
      prefix = prefix + "looks-detail?nid=" + itemId
    } else if (pageType == "product") {
      prefix = prefix + "product-detail?product_id=" + itemId
    } else if (pageType == "blog" || pageType == "article") {
      prefix = prefix + "model-says?nid=" + itemId
    }else if (pageType == "stylepro"){
       prefix = prefix + "stylepro/" + itemId
    }
  } else {
      var currentPath = window.location.href.split("#/app/")[1];
      prefix = prefix + currentPath
  }

  return prefix
}

var apkDownloadLink = getShareUrl();
var appleDownloadLink = getShareUrl();


// var apkDownloadLink = 'Please download the app from the given link, https://play.google.com/store/apps/details?id=com.discernliving.app';
// var appleDownloadLink = 'Please download the app from the given link, https://itunes.apple.com/in/app/discern-living/id1191273950?mt=8';

var FCM_key = 'AAAAVFPv0u8:APA91bHymuDb5W027Gl68tLyi9_WkZcFvoI2v3lR9QXlFEX0ZXd3Z44crM-Xc8XmNKVFxn0l1_SAgjqVpsSj0SbjALJNRUoYbm3yb_4XIdqUHYYmqd0_W9MEGOqGuLi2xLBXA2O_AfE6';
/**
 *@description:This function is used to show page loading
 * */
appModule.constant('$ionicLoadingConfig', {
  template: ' '//'<div class="discern-loader">' + getLoader() + '</div>'
});


var getUser = () => localStorage.getItem('userEmail') || "Guest"
var lastCartId, googleGA = {
  gaView: viewName => analytics.trackView(viewName, getUser()) // viewName => console.log(viewName)
};
