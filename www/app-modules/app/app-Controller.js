/**
 * todo : Need to do this using requireJS
 * */
appModule.controller('AppCtrl', function ($nlFramework, $scope, $ionicModal, $timeout, $ionicLoading, $rootScope, $http, $ionicPopup, $ionicScrollDelegate, DataFactory, ngFB, $location, interceptorFunction, $window, $ionicSlideBoxDelegate, $state, discoverService, ListingService, looksService, UtilityService, $ionicSideMenuDelegate, $ionicPlatform, $document, $ionicGesture, CartService, $stateParams, searchService, makeOverService, builderService, $q, loginService, quizService, $cordovaGoogleAnalytics, $ionicPush, $ionicAuth, $ionicFacebookAuth, $ionicUser) {

  UtilityService.disableLog();
  var successHits = 0;
  var loadMinorItem = false;
  var user = {};
  var currentVal = 2;
  $scope.beforeLoginState = "";
  $scope.loginTab = 1;
  $rootScope.combine = '';
  $scope.isShowSearch = false;
  $scope.showThis = false;

  $scope.goToBuilder = function (nid, builderTid) {
    $scope.hideSideMenu();
    $rootScope.builderTid = builderTid;
    $state.go('app.builder', { nid: nid });
  };

  $scope.reportAppLaunched = function(url) {
		// console.log("App Launched Via Custom URL");
    // console.log(url);

		// $scope.$apply( function() {
		// 	$scope.data.launched = "Yes";
	  // });
	}


  $rootScope.goToMakeover = function () {
    checkMkOverStatus('fromMenu');
  };

  $scope.getScrollPosition = function () {
    $scope.showArrow = 200;
    $scope.scrollVpos = $ionicScrollDelegate.$getByHandle('handler').getScrollPosition().top;
    $scope.scrollArrow = $scope.scrollVpos <= $scope.showArrow ? false : true;
  }

  $scope.showBuilders = function () {
    //$ionicLoading.show();
    $scope.showLoading = true;
    builderService.getBuilderMenu().then(function (getBuilders) {
      $scope.builders = getBuilders;
      $rootScope.isBuildersMenuActive = !$rootScope.isBuildersMenuActive;
      // $ionicLoading.hide();
      $scope.showLoading = false;
      $scope.scrollDown();
    });

  };

  $scope.scrollDown = function () {
    $ionicScrollDelegate.scrollBottom([true]);
  }

  $scope.setBuilderLogo = function (index) {
    $rootScope.builderLogo = $scope.builders[index].builder_logo;
  }

  $scope.showTokenPanel = function () {
    $scope.hideSideMenu();
    showBuilderTokenPanel();
  };
  // $scope.tab = 0;
  //   $scope.changeTab = function(newTab){
  //     $scope.tab = newTab;
  //   };
  //   $scope.isActiveTab = function(tab){
  //     return $scope.tab === tab;
  //   };
  /***********/
  /**
   * Show quiz items in random order
   * */

  //UtilityService.disableConsoleDebug();
  $scope.random = function () {
    return 0.5 - Math.random();
  };

  $scope.discoverPopShown = false;

  $ionicGesture.on('dragend', function (e) {
    $timeout(function () {
      $rootScope.$broadcast('sideMenuOpened');
    }, 30);
  }, $document);

  ngFB.init({
    appId: '988341247854562'
  });

  /**
   * @description: This function is called on clicking back button of a mobile phone
   * */

  $ionicPlatform.onHardwareBackButton(function () {
    $scope.modalLogin.hide();
    if ($rootScope.isNewMenuOpen) {
      $scope.hideSideMenu();
    }
    $ionicLoading.hide();
    $rootScope.$emit('hardwareBackBtnPressed');
  });

  $rootScope.scrollMe = function (left, top, animation) {
    $ionicScrollDelegate.scrollTo(left, top, animation);
    $scope.scrollArrow = false;

  }

  // initialize the whole framework
  // Options
  //
  var nlOptions = {
    // global settings
    speed: 0.2,
    animation: 'ease',
    // use toast messages
    toast: true,
    // burger specific
    burger: {
      use: false,
      endY: 6,
      startScale: 1, // X scale of bottom and top line of burger menu at starting point (OFF state)
      endScale: 0.7 // X scale of bottom and top line of burger menu at end point (ON state)
    },
    // drawer specific
    drawer: {
      maxWidth: 400,
      openCb: function () {
        $rootScope.$apply(function () {
          $rootScope.isNewMenuOpen = true;
        });
      },
      closeCb: function () {
        $rootScope.$apply(function () {
          $rootScope.isNewMenuOpen = false;
        });
      }
    }
  };
  // initialize the framework
  $nlFramework.init(nlOptions);




  $scope.hideSideMenu = function () {
    $scope.toggleMenu();
  };

  // scope: 'email,read_stream,publish_actions'

  $scope.flipImage = function (value) {
    $scope["isActive" + value] = !$scope["isActive" + value];
  };


  /**
   * @description : Toggle Side Menu
   * */
  $scope.toggleMenu = function () {



    if (!$rootScope.isNewMenuOpen) {
      $nlFramework.drawer.show();
    } else {
      $nlFramework.drawer.hide();
    }
    // $ionicSideMenuDelegate.toggleLeft();
    //$ionicSideMenuDelegate.canDragContent(false);
    $rootScope.isNewMenuOpen = !$rootScope.isNewMenuOpen;
    $rootScope.isBuildersMenuActive = false;
  };
  // * if given group is the selected group, deselect it
  // * else, select the given group
  // accordion scope function($scope
  $scope.toggleGroup = function (group) {
    // //console.log(group);
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };

  $scope.isGroupShown = function (group) {
    return $scope.shownGroup === group;
  };

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('app-modules/login/login.html', {
    scope: $scope,
    hardwareBackButtonClose: true
  }).then(function (modal) {
    $scope.modalLogin = modal;
  });

  /**
   * @description : Handle Projects here
   * */
  $scope.toggleProjects = function () {
    $scope.isProjectSelected = !$scope.isProjectSelected;
  };

  // Triggered in the login modal to close it
  $scope.closeLogin = function (force, closeBtnClicked) {


    // reset form new to be done
    /*$scope.theUser = angular.copy(loginForm);
     ///$scope.loginForm.submitted = false;

     loginForm.$setValidity(true);
     loginForm.email.$setPristine();
     loginForm.email.$setUntouched();*/
    // reset form end


    $scope.modalLogin.hide();
    $scope.hidePsw = false;
    $scope.resetEmail.id = "";
    $scope.loginData.email = "";
    $scope.loginData.password = "";
    $scope.loginData.firstname = "";
    $scope.loginData.lastname = "";
    $scope.showLogin();

    if ($scope.beforeQuiz && force) {
      $scope.closeQuiz();
      $scope.isQuizStarted = false;
      resetQuizDetailsForDiscover();
    }
    if ($ionicSideMenuDelegate.isOpenLeft()) {
      $ionicSideMenuDelegate.toggleLeft();
    }

    if ($scope.LoginFromQuiz == true && closeBtnClicked) {
      localStorage.removeItem('quizCount');
      localStorage.removeItem('personaMajor');
      localStorage.removeItem('personaMinor');
      localStorage.removeItem('majorResultText');
      localStorage.removeItem('minorResultText');
      localStorage.removeItem('majorPersonaImage');
      localStorage.removeItem('minorPersonaImage');
      localStorage.setItem('quizPlayed', '0');
      $scope.quizPlayed = false;
      discoverService.getUserPersona()
      //$rootScope.$emit('showProCta')
      $scope.LoginFromQuiz = false
      $scope.crossClicked == false
      $rootScope.$emit('discoverLogout')

    }


  };
  // Open the login modal
  $scope.login = function () {
    $scope.modalLogin.show();
  };

  $scope.showChngPswd = function () {
    $scope.loginTab = 3;
  };
  $scope.showfgtPswd = function () {
    $scope.loginTab = 4;
  };
  $scope.showSignUp = function () {
    $scope.loginTab = 2;
    $scope.loginData.email = "";
    $scope.loginData.password = "";
    $scope.loginData.firstname = "";
    $scope.loginData.lastname = "";
  };

  $scope.showLogin = function () {
    $scope.loginTab = 1;
    $scope.loginData.email = "";
    $scope.loginData.password = "";
    $scope.loginData.mobile = "";
    if ($scope.loginFormElem) {
      $timeout(function () {
        $scope.loginFormElem.email.$setValidity("required", true);
        $scope.loginFormElem.password.$setValidity("required", true);
        $scope.loginFormElem.$setPristine();
      });
    }
  };

  $scope.inValidMRP = function (discount, mrp) {

    discount = typeof discount == "string" ? parseFloat(discount) : discount
    mrp = typeof mrp == "string" ? parseFloat(mrp) : mrp

    if (!mrp) {
      return true
    } else if (!discount) {
      return false
    } else if (discount > mrp) {
      return true
    } else {
      return false
    }
  }

  $scope.isDiscounted = function (discounted, mrp) {
    if (mrp && typeof mrp == 'string') { mrp = parseFloat(mrp.replace(/[,]/g, "")); }
    if (discounted && typeof discounted == 'string') { discounted = parseFloat(discounted.replace(/[,]/g, "")); }

      if (discounted) {
            if (discounted < mrp) {
            return true;
            } else {
              return false;
            }
      }else{
        return false
      }
  }

  // Open the login modal
  $scope.logOut = function () {
    var token = localStorage.getItem("userToken");
    $ionicLoading.show();

    $http({
      method: 'POST',
      url: login_urls.user_logout,
      withCredentials: true,
      crossDomain: true,
      headers: {
        //  "Content-Type": "text/html; charset=UTF-8'",
        //"Cookie":localStorage.getItem('userSessionName') + "=" + localStorage.getItem('userSessionId'),
        'Content-Type': 'text/html; charset=UTF-8',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Expose-Headers': '*'
      }
    }).then(function (result) {
      $ionicLoading.hide();
      resetAfterLogut();
      $rootScope.$emit("updateLooks");

    //  $ionicFacebookAuth.logout(); // fb logout from ionic server
     // $ionicAuth.logout(); // logout from ionic server
    }, function (response) {
      $ionicLoading.hide();
      resetAfterLogut();
      //            $rootScope.$emit("serviceMsg", 'Unable to logout.');
    });
    $rootScope.$emit('discoverLogout');
    $rootScope.$emit('resetMkOutSide');

    // if user was logged in from facebook
    if (localStorage.getItem('loggedInFromFb')) {
      $ionicLoading.show();
      facebookConnectPlugin.logout(function () {
        $ionicLoading.hide();
        localStorage.removeItem('loggedInFromFb')
        $state.go('app.feed');
      },
        function (fail) {
          $ionicLoading.hide();
        });
    }

    // if user was logged in from google
    // if (localStorage.getItem('loggedInFromFb')) {
    //   $ionicLoading.show();
    //   window.plugins.googleplus.logout(
    //     function (msg) {
    //       console.log(msg);
    //       $ionicLoading.hide();
    //       localStorage.removeItem('loggedInFromFb')
    //       $state.go('app.feed');
    //     },
    //     function (fail) {
    //       console.log(fail);
    //     }
    //   );
    // }

  };

  var resetQuizDetailsForDiscover = function () {
    if (!discoverService.quizPlayed) {
      localStorage.removeItem("majorPersonaImage");
      localStorage.removeItem("majorResultText");
      localStorage.removeItem("minorPersonaImage");
      localStorage.removeItem("minorResultText");
      localStorage.removeItem("personaMajor");
      localStorage.removeItem("personaMinor");
      localStorage.removeItem("quizCount");
      localStorage.removeItem("quizPlayed");
      // discoverService.quizPlayed = localStorage.getItem("quizPlayed") == true;
      $scope.quizPlayed = false;
      looksService.isPersonaAvailable = false;
    }
  };

  var resetAfterLogut = function () {
    $state.go('app.feed');
    $scope.loginData = {};
    $scope.name = "";
    $scope.loginTab = 1;
    $rootScope.userPicture = "img/d-logo-quiz.png";
    discoverService.quizPlayed = localStorage.getItem("quizPlayed") == true;
    $rootScope.majorResultText = "";
    $rootScope.minorResultText = "";
    $rootScope.userEmail = "";
    $scope.quizPlayed = false;
    //$ionicSideMenuDelegate.toggleLeft();
    localStorage.clear();
    looksService.isPersonaAvailable = false;
    CartService.cart = [];
    delete CartService.prodIds;
    CartService.prodQuantities = [];
    $rootScope.cartCount = 0;

    CartService.getCartSummary();

  };




  // scope checks for the sidemenu opened ratio
  // QUIZ MODAL STARTS
  // Create the Quiz modal that we will use later
  $ionicModal.fromTemplateUrl('app-modules/quiz/quiz.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modalQuiz = modal;
  });

  $ionicModal.fromTemplateUrl('app-modules/popup_modal/quiz_discover.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.DiscoverModalQuiz = modal;
  });

  $scope.closeDiscoverQuiz = function () {
    $ionicLoading.hide();
    $scope.DiscoverModalQuiz.hide();
    $scope.error = true;
  };

  $scope.pageSection = 1;
  $scope.tab = 1;
  // $timeout(function() {
  //     $scope.modalQuiz.show();
  //      $scope.updateChair();
  // }, 100);
  // Open the quiz modal
  $scope.playQuiz = function (playFromStep) {
    $scope.quizItems = dummyQuiz;
    $scope.showLoading = false;

    $scope.fremFeed = $state.current.name == "app.feed";

    playFromStep == undefined ? playFromStep = 1 : $scope.isQuizStarted = true
    $scope.modalQuiz.show();
    updateQuizTemplate('sofa');
    $scope.pageSection = playFromStep;
    $scope.tab = 1;
    $scope.error = true;
    $('.popBackground').css('display', 'none');
    $('.modal-backdrop, .modal-backdrop-bg').css('position', 'inherit');
    $('.view-container').show();
  };

  // Triggered in the playQuiz modal to close it
  // destination : if user come from makeover Quiz
  $scope.closeQuiz = function (fromHTML) {

    if(fromHTML && UtilityService.getDeviceInfo().id != '12233434') { analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Ignored');}

    $ionicLoading.hide();
    $scope.modalQuiz.hide();
    //$scope.showThis = false;

    //        $scope.pageSection = 1;
    //        $scope.tab = 1;
    if (localStorage.getItem("personaMajor") !== null && $state.current.name != 'app.design-makeover-styleQuiz') {
      $state.go("app.discover");
    } else if (localStorage.getItem("personaMajor") !== null && $state.current.name == 'app.design-makeover-styleQuiz') {
      // dont chnage stage stay on makeover
    }
    discoverService.quizPlayed = localStorage.getItem("quizPlayed") == true;
    $scope.currentSectionName = "sofa";
    $scope.quizStatus = { name: "sofa", header: "sofa-icon-head", footer: "sofa-icon-footer", footerText: "NEXT" };
    currentVal = 2;
    clearQuizScope();
    clearSelectedQuizData();
    $scope.selectedQuizData.chair = [];
    $scope.selectedQuizData.sofa = [];
    $scope.selectedQuizData.table = [];
    $scope.selectedQuizData.lamps = [];
    $scope.selectedQuizData.looks = [];
    quizService.updateQuizMsg();
    $('.view-container').show();
    $ionicSlideBoxDelegate.$getByHandle("recommendedFeed").update();


  };


  // INTRO MODAL STARTS

  $scope.dev_height = $window.innerHeight;

  $ionicModal.fromTemplateUrl('app-modules/popup_modal/intro.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modalIntro = modal;
    $ionicLoading.hide();
  });

  // Called each time the slide changes
  $scope.slideChanged = function (index) {
    //console.log($ionicSlideBoxDelegate.slideChanged());
    $scope.slideIndex = index;
    var totalSlides = 5; //totalSlides is 6(0-5)
    if (index === totalSlides) {
      $scope.closeIntro();
    }
    $('.slider-slide').find('.box').removeClass('active');
    $('.slider-slide').eq(index).find('.box').addClass('active');
    //var myItem = $('.introSlider').find('.box').eq(index);
    // myItem.find('.intro-hdr').addClass('animated zoomInBg');
    //   myItem.find('.intro-title, p').addClass('animated slideInLeft');
    //  myItem.find('.intro-cta').addClass('animated flipUp');

    // $('.introSlider').find('.box').eq(index).find('.intro-hdr').addClass('animated zoomInBg');
    // $('.introSlider').find('.box').eq(index).find('.intro-title, p').addClass('animated slideInLeft');
    // $('.introSlider').find('.box').eq(index).find('.intro-cta').addClass('animated flipUp');
    // $('.slider-pager span:last-child').hide(); handled in css into.css
  };

  // Open the Intro modal
  $scope.showIntro = function () {
    $scope.modalIntro.show();
  };

  // Triggered in the intro modal to close it
  $scope.closeIntro = function () {
    if (localStorage.getItem("quizShown") === null) {
      initLoginNQuiz();
    }
    $scope.modalIntro.hide();
    $state.go('app.feed');
    $rootScope.hideMainMenu = true;
    $timeout(function () {
      $ionicSlideBoxDelegate.previous();
      $ionicSlideBoxDelegate.previous();
      $ionicSlideBoxDelegate.previous();
    }, 500);
  };

  /**
   * @description : This function used for show hide page sections (start screen, items tab, final results)
   * */

  $scope.sectionQuiz = function (value) {

    /**
     * @description: this code is uded to track events in google analytics
     * */
    // if(typeof analytics !== "undefined"){
    switch ($state.current.name) {
      case 'app.feed':
        if(!$scope.fremFeed){
            console.log('HOW');
            if (UtilityService.getDeviceInfo().id != '12233434'){
            analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Started post how it works');}
        }else{
            $scope.fremFeed = false;
            console.log('FEED');
            if (UtilityService.getDeviceInfo().id != '12233434'){
            analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Started from feed');}
        }
        break;
      case 'app.discover':
        if (UtilityService.getDeviceInfo().id != '12233434'){
        analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Started from Discover Page');}
        break;
      default:
        break;
    }
   //  }



    // $scope.pageSection = value;
    $scope.isQuizStarted = true;
    $scope.showThis = false;


    switch (value) {
      case 2:
        $scope.pageSection = value;
        break;
      case 3:
        if ($scope.quiz_dataObject.length > 0) {
          $scope.pageSection = value;
          // $('.scroll').css('transform', ' translate3d(0px, 0px, 0px) scale(1)');

          $scope.updateLooks();
        } else {
          $rootScope.$emit("serviceMsg", 'Select atleast one product');
        }

        break;
      case 4:
        if ($scope.selectedQuizData.looks.length > 0) {
          //$ionicLoading.show();
          $scope.showLoading = true;
          postQuizInfo();
        } else {
          $rootScope.$emit("serviceMsg", 'Select atleast one product');
        }

        break;
    }

    $ionicScrollDelegate.scrollTop();
  };
  /**
   *@description:this Function is used to share  screenshot in discover result
   * */
  $scope.shareIt = function () {
    var dResult = angular.element('.shareCnva .style');
    dResult.removeClass('startAni');
    $scope.snapInProgress = true;
    $scope.loadingDiscoverShare = true;
    // $ionicLoading.show();
    $timeout(function () {
      html2canvas(angular.element('.shareCnva .style'), {
        useCORS: true,
        height: angular.element('.shareCnva .style').height(),
        onrendered: function (canvas) {
          $http.post(shareDisURL, [
            {
              uid: localStorage.getItem('userUid'),
              uname: localStorage.getItem('userName'),
              title: new Date().valueOf(), // localStorage.getItem('userName'),
              img: canvas.toDataURL("image/jpeg")
            }
          ]).then(function (response) {
            $scope.snapInProgress = false;
            $scope.socializeMe(response.data.image);
            //$scope.socializeMe(response.data.image.replace("public://", imgPath));
          });
        }
      })
    }, 100);
  };
  /**
   * @description : Open Social Sharing
   * */
  $scope.socializeMe = function (imageUrl, msg) {
    if (typeof window.plugins !== "undefined") {

      msg = msg ? msg : "I'm "+localStorage.getItem('majorResultText')+" "+localStorage.getItem('minorResultText')+"! Take Discern Living's fun quiz to find out what design style you are! "

      UtilityService.getShortUrl().then(function(response){
            window.plugins.socialsharing.share(msg + response.data.id, 'Discern living', imageUrl, function () {
              // $ionicLoading.hide();
              $scope.loadingDiscoverShare = false;
            }, function () {
              //            $rootScope.$emit("serviceMsg", '<p>Sorry, some error occured !</p><p>Please try again.</p>');
              $scope.loadingDiscoverShare = false;
              // $ionicLoading.hide();
            });
      });
    } else {
      $scope.loadingDiscoverShare = false;
      $rootScope.$emit("serviceMsg", "<p>Social Sharing is not available on your device.</p>");
      // $ionicLoading.hide();
    }
    // $ionicLoading.hide();
  };

    /**
   * @description: This function is used to send discover result images to server for share
   * */
  self.saveDrImage = function (canvas) {
    /* $http.post(shareURL, [
     {
     uid: utilityService.getUserInfo().uid,
     uname: utilityService.getUserInfo().uName,
     title: 'title',
     img: canvas.toDataURL("image/jpeg")
     }
     ]).then(function (response) {


     });*/
  };

  var dummyQuiz = {
    "0": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "0": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "2": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "3": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "4": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "5": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "6": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "7": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "8": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "9": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "10": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "11": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "12": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "13": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "14": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    },
    "15": {
        "quiz_id": "0",
        "product_id": "0",
        "image": "",
        "style": "0"
    }
};

  /**
   * @description : This function used for show/hide tabs sections (chair,bed,sofa,other)
   * */
 $scope.sectionTab = function () {
    var isUpdate = false;
    // $scope.tab = value;
    var showAlert = () => $rootScope.$emit("serviceMsg", 'Select your top 3 choices');
    $scope.showLoading = true;
    switch (currentVal) {
      case 2:
        if (!($scope.selectedQuizData.sofa.length < 3)) {
          // if (!($scope.selectedQuizData.sofa.length < 3) || $scope.itemsSofa.length == 0) {
          // $scope.updateTable().then(function () {
            $scope.tab = currentVal;
            $scope.quizItems = dummyQuiz;
            updateQuizTemplate('table').then(() => $scope.showLoading = false);
            ++currentVal;
            $scope.quizStatus = {
              name: "table",
              header: "table-icon-head",
              footer: "table-icon-footer",
              footerText: "NEXT"
            };
            $scope.currentSectionName = "table";
          clearQuizScope();
          isUpdate = true;
        } else {
          $scope.showLoading = false
          showAlert();
        }
        break;
      case 3:
        if (!($scope.selectedQuizData.table.length < 3)) {
          // $scope.updateChair().then(function () {
            $scope.tab = currentVal;
            $scope.quizItems = dummyQuiz;
            updateQuizTemplate('chair').then(() => $scope.showLoading = false);
            ++currentVal;
            $scope.quizStatus = {
              name: "chair",
              header: "chair-icon-head",
              footer: "chair-icon-footer",
              footerText: "NEXT"
            };
            $scope.currentSectionName = "chair";

          clearQuizScope();
          isUpdate = true;
        } else {
          $scope.showLoading = false
          showAlert();
        }
        break;
      case 4:
        if (!($scope.selectedQuizData.chair.length < 3)) {
          // $scope.updateLamps().then(function () {
            $scope.tab = currentVal;
            $scope.quizItems = dummyQuiz;
            updateQuizTemplate('lamps').then(() => $scope.showLoading = false);
            ++currentVal;
            $scope.quizStatus = {
              name: "lamp",
              header: "lamp-icon-head",
              footer: "lamp-icon-footer",
              footerText: "NEXT"
            };
            $scope.currentSectionName = "lamp";

          clearQuizScope();
          isUpdate = true;
        } else {
          $scope.showLoading = false;
          showAlert();
        }
        break;
      case 5:
        if (!($scope.selectedQuizData.lamps.length < 3)) {
          // $scope.updateLooks().then(function () {
            $scope.tab = currentVal;
            $scope.quizItems = dummyQuiz;
            updateQuizTemplate('looks').then(() => $scope.showLoading = false);
            ++currentVal;
            $scope.quizStatus = {
              name: "look",
              header: "look-icon-head",
              footer: "look-icon-footer",
              footerText: "NEXT"
            };
            $scope.currentSectionName = "look";

          clearQuizScope();
          isUpdate = true;
        } else {
          $scope.showLoading = false
          showAlert();
        }
        break;
      case 6:
        if (!($scope.selectedQuizData.looks.length < 3)) {
          clearQuizScope();
        $scope.sectionQuiz(4);
        } else {
          $scope.showLoading = false
          showAlert();
        }
    }
  };

  /**
   * @description : This function used for creating selected object array
   * */

  $scope.selectItem = function (value, style, type) {

    if(value != '0') {
    var valueIndex = {};

    var type = type =='lamp' ? 'lamps' : type,

      obj = type == 'object' ? $scope.quiz_dataObject : $scope.selectedQuizData[type]

    valueIndex = objectFindByKey(obj, 'product_id', value);
    if (valueIndex != null) {
      $scope["isActive" + value] = false;
      removeElement(obj, valueIndex);
    } else {
      if (!checkMaxSelection(type)) {
        obj.push({
          product_id: value,
          style: style
          // ,type: type
        });
        $scope["isActive" + value] = true;
      }
    }}




  };
  /**
   * @description : update message
   * */
  var checkMaxSelection = function (key) {
    var isMax = false;
    if ($scope.selectedQuizData[key].length >= 3) {
      $rootScope.$emit("serviceMsg", 'Max 3 items could be selected.');
      isMax = true;
    }
    return isMax;
  };

  /**
   * @description : This function used for finding object in quiz items array
   * */
  var objectFindByKey = function (array, key, value) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] === value) {
        return i;
      }
    }
    return null;
  };

  /**
   * @description : This function used for removing object in array
   * */
  var removeElement = function (array, value) {
    array.splice(value, 1);
  };

  var quiz_id = 0;
  var quizProductUrl = function (quiz_id, prodType) {

    return '/gitdiscern-quiz/quiz/retrieve?nitems=' + quiz_id + '&since=0&type=' + prodType
  }
  var updateQuizTemplate = function (prodType) {
   // $ionicLoading.show();
    return quizService.getService(quizProductUrl(quiz_id, prodType)).then(function (response) {

      $scope.quizItems = shuffle(convertToArray(response.data));
      if ($scope.quizItems.length > 0) {
        quiz_id = !quiz_id ? $scope.quizItems[0].quiz_id : quiz_id
        $ionicScrollDelegate.scrollTop();
        $scope.quizItems
      } else {
        $scope.closeQuiz()
        $rootScope.$emit('serviceMsg', 'Quiz is not available')
      }
    //  $ionicLoading.hide();
    });
  }

  var postQuizWithLogInInfo = function () {
    submitPersona($rootScope.selectedTiePerson);
    delete $rootScope.selectedTiePerson
    /* return $http.post("gitdiscern-user-quiz-result/user-update-quiz-result.json", {
         uid: localStorage.getItem('userUid'),
         device_id: UtilityService.getDeviceInfo().id
       });*/

  };

  var postQuizInfo = function () {
    var uid = localStorage.getItem('userUid') !== null ? localStorage.getItem('userUid') : UtilityService.getDeviceInfo().id;

    postQuizData(uid, 'sofa');
    postQuizData(uid, 'table');
    postQuizData(uid, 'chair');
    postQuizData(uid, 'looks');
    postQuizData(uid, 'lamps');
    clearQuizScope();
    localStorage.setItem('quizPlayed', true);

  };

  /**
   * @description : This function used for sending checked values to server for Logged in users
   * */

  var checkLogin = function (from) {

    $scope.LoginFromQuiz = from == 'LoginFromQuiz' ? true : false

    if (!$scope.isLoggedIn()) {
      $scope.login();
      $scope.beforeQuiz = true;
      $ionicLoading.hide();
      return false;

    } else {

      return true;
    }
  };

  var clearQuizScope = function () {
    var valueToPass;
    $.each($scope, function (key) {
      /* iterate through array or object */
      if (key.match(/isActive/g)) {
        valueToPass = parseInt(key.replace(/isActive/, "").trim());

        if (!isNaN(valueToPass)) {
          $scope["isActive" + valueToPass] = false;
        }
      }
    });
  };

  var clearSelectedQuizData = function () {
    $scope.selectedQuizData = { lamps: [], table: [], chair: [], sofa: [], looks: [] }
  }

  /**
   * @description : This function used for send selected quiz items
   * */

  var postQuizData = function (uid, itemType) {

    var allItems = Object.keys($scope.selectedQuizData),
      currentItem = allItems[allItems.indexOf(itemType)],
      currentSelectedItems = $scope.selectedQuizData[currentItem]

    var quizDataToPost = '{"uid":"' + uid + '","quiz_id": "' + quiz_id + '", "type": "' + itemType + '","quiz_data":' + JSON.stringify(currentSelectedItems) + '}';

    quizService.postService(quizURLs.userQuizData, quizDataToPost).then(function (data) {
      $scope.selectedQuizData[currentItem] = [];
      localStorage.setItem('quizPlayed', true);
      //console.log(data);
      successHits++;
      findQuizResponse(uid);
      $scope.showLoading = false;
    });

  }
  /**
   * @description : This function used for retriving quiz result
   * */

  var findQuizResponse = function (uid) {
    $scope.showLoading = true;
    if (successHits === 5) {
      var postDataObject = '{"uid":"' + uid + '","quiz_id": "' + quiz_id + '"}';

      quizService.postService(quizURLs.quizResult, postDataObject).then(function (data) {
        localStorage.setItem('quizPlayed', '1');
        //console.log(data);
        successHits = 0;
        showResult(data.data);
        $scope.showLoading = false;
      });

    }
  };

  /**
   * @description : This function checks for the cases [final persona value or initiates tie break ]
   * */
  var showResult = function (data) { // {uid: "39", quiz_id: "5", highest_major_style: 45, highest_minor_style: 48}

    var quizCount = parseInt(localStorage.getItem('quizCount'));
    if (quizCount != null) {
      quizCount++;
      localStorage.setItem('quizCount', quizCount);
    }
    var major = typeof data.major_style !== "undefined" ? parseInt(data.major_style) : parseInt(data.highest_major_style);
    var minor = typeof data.minor_style !== "undefined" ? parseInt(data.minor_style) : parseInt(data.highest_minor_style);
    $scope.isMajorTieBreak = (!major && !minor);

    if (!major) {
      $scope.modalTieBreak.show();
      $scope.itemsTieBreak = { items: data.major_style_data, type: "major" };
      $ionicLoading.hide();
      if ($scope.isMajorTieBreak) {
        $scope.currentTieBreakStatus = "start";
        $scope.itemsMinorTieBreak = data.minor_style_data;
        $scope.quizTieBreakTxt = { header: "Nearly done!", footer: "Next" };

      }

    } else if (!minor) {
      $scope.modalTieBreak.show();
      $scope.itemsTieBreak = { items: data.minor_style_data, type: "minor" };

      $ionicLoading.hide();

    } else {
      checkLogin('LoginFromQuiz');

    }
    updateMajorResult(data);
    updateMinorResult(data);

    if ($state.current.name == 'app.discover') {
      $scope.$emit('getStyleQproducts', minor, major);
    }


    // if submit from makeover if no TieBreak
    if ($state.current.name == 'app.design-makeover-styleQuiz' && major != false && minor != false) {
      $scope.$emit('submitFinalMakeoverData');
    }

    $scope.quizPlayed = true;
    quizService.updateQuizMsg();
    $rootScope.$emit('discoverLogin');
    $scope.closeQuiz();
  };

  function updateMajorResult(data) {
    var major = typeof data.major_style !== "undefined" ? parseInt(data.major_style) : parseInt(data.highest_major_style);
    switch (major) {
      case 32: //35:
        // Classic =
        $scope.majorResult = "Classic";
        break;
      case 33: //40:
        // Modern =
        $scope.majorResult = "Modern";
        break;
      case 34: //45:
        // Ethnic =
        $scope.majorResult = "Ethnic";
        //loadPersonaImage();
        break;
      case 230: //232:
        // Ethnic =
        $scope.majorResult = "Trendy";
        //loadPersonaImage();
        break;
    }
    localStorage.setItem("personaMajor", typeof major !== "undefined" ? major : "32");
    localStorage.setItem("majorResultText", $scope.majorResult);
    localStorage.setItem("majorPersonaImage", data.major_image);
    $rootScope.majorResultText = $scope.majorResult;
    $rootScope.majorPersonaImage = data.major_image;
  }

  function updateMinorResult(data) {
    var minor = typeof data.minor_style !== "undefined" ? parseInt(data.minor_style) : parseInt(data.highest_minor_style);
    switch (minor) {
      case 36:
        // Glamour =
        $scope.minorResult = "Glamour";
        break;
      case 37:
        // Industrial =
        $scope.minorResult = "Industrial";
        break;
      case 38:
        // Country / Shabby Chic =
        $scope.minorResult = "European";
        break;
      case 39:
        // Eclectic =
        $scope.minorResult = "Eclectic";
        break;
      case 41:
        // Contemporary =
        $scope.minorResult = "Contemporary";
        break;
      case 42:
        // Design =
        $scope.minorResult = "Design";
        break;
      case 43:
        // Mid - Century =
        $scope.minorResult = "Mid - Century";
        break;
      case 44:
        // Scandinavian =
        $scope.minorResult = "Scandinavian";
        break;
      case 46:
        // Bollywood =
        $scope.minorResult = "Indian";
        break;
      case 47:
        // Mughal =
        $scope.minorResult = "Middle Eastern";
        break;
      case 48:
        // Colonial =
        $scope.minorResult = "Colonial";
        break;
      case 49:
        // Far East =
        $scope.minorResult = "Asian";
        //loadPersonaImage();
        break;
      case 231:
        // Far East =
        $scope.minorResult = "Rustic";
        //loadPersonaImage();
        break;
    }
    localStorage.setItem("personaMinor", minor);
    localStorage.setItem("minorResultText", $scope.minorResult);
    localStorage.setItem("minorPersonaImage", data.minor_image);
    $rootScope.minorResultText = $scope.minorResult;
    $rootScope.minorPersonaImage = data.minor_image;
  }

  /**
   * @description : Create the Quiz tieBreak modal that we will use later
   * */
  $ionicModal.fromTemplateUrl('app-modules/popup_modal/quiz_tieBreak.html', {
    scope: $scope
  }).then(function (modal) {
    modal.el.style.zIndex = '9';
    $scope.modalTieBreak = modal;
  });

  /**
   * @description : This function is used to make one active check on page
   * */

  $scope.radioSelect = function (group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
      $rootScope.tieBreakSelected = 0;
    } else {
      $scope.shownGroup = group;
      $rootScope.tieBreakSelected = group;
    }
    ////console.log($rootScope.tieBreakSelected);
  };

  /**
   * @description : This function is used to submit persona in case of tie break
   * */
  $scope.submitTieBreak = function () {
    $scope.showLoading = true;

    if ($rootScope.tieBreakSelected === 0) {
      $rootScope.$emit("serviceMsg", 'Select atleast one product');
    } else {

      /*if (checkMajorMinor($rootScope.tieBreakSelected)) {
       if ($rootScope.minorTieObject.length > 0) {
       $scope.itemsTieBreak = $rootScope.minorTieObject;
       $rootScope.minorTieObject = '';
       } else {
       // if (!loadMinorItem) {
       localStorage.setItem("personaMajor", typeof $rootScope.tieBreakSelected !== "undefined" ? $rootScope.tieBreakSelected : "32");
       // } else {
       // localStorage.setItem("personaMajor", $rootScope.tieBreakSelected);
       loadMinorItem = false;
       // }
       }
       } else {
       if ($rootScope.minorTieObject.length > 0) {
       $scope.itemsTieBreak = $rootScope.minorTieObject;
       $rootScope.minorTieObject = '';
       } else {
       // if (!loadMinorItem) {
       localStorage.setItem("personaMinor", $rootScope.tieBreakSelected);
       // } else {
       // localStorage.setItem("personaMajor", $rootScope.tieBreakSelected);
       loadMinorItem = false;
       // }
       }
       }*/

      submitPersona($rootScope.tieBreakSelected);
      $rootScope.tieBreakSelected = 0;
      $scope.shownGroup = null;


    }

  };

  /**
   * @description : This function is used to check if a user is logged-in and then direct to projects page
   * */
  $scope.goToProjects = function () {
    if ($scope.isLoggedIn()) {
      $state.go("app.projects-landing");
    } else {
      $scope.beforeLoginState = "app.projects-landing";
      $scope.login();
    }
  };

  var noTieBreak = function () {


    var uid = localStorage.getItem('userUid') !== null ? localStorage.getItem('userUid') : UtilityService.getDeviceInfo().id;
    //        if (!loadMinorItem) {
    //$ionicLoading.show();
    $scope.showLoading = true;
    var postData = { uid: uid, quiz_id: quiz_id };

    $http.post("gitdiscern-user-quiz-tie-breaker-result/user-quiz-result.json", postData).then(function (response) {

     // $ionicLoading.hide();

      if(!localStorage.getItem('quizPlayed')){
        analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Completed Once');
        }else{
        analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Completed more than once');
        }
      localStorage.setItem('quizPlayed', '1');
      $scope.quizPlayed = true;
      discoverService.quizPlayed = true;


      // if submit from makeover submit makeover form too
      if ($state.current.name == 'app.design-makeover-styleQuiz' && localStorage.getItem('makeOverSubmited') == null && localStorage.getItem('personaMajor') != false && localStorage.getItem('personaMinor') != false) {
        $scope.$emit('submitFinalMakeoverData');
      }
      quizService.updateQuizMsg();
      $rootScope.$emit('discoverLogin');

      $scope.showLoading = false;

    });

  }

  /**
   * @description : This function is used to submit final persona to server
   * */
  var submitPersona = function (value) {


    if (!$scope.isLoggedIn()) {
      checkLogin("LoginFromQuiz");
      $rootScope.selectedTiePerson = value;

    } else {

      // loadMinorItem = false;
      var uid = localStorage.getItem('userUid') !== null ? localStorage.getItem('userUid') : UtilityService.getDeviceInfo().id;
      //        if (!loadMinorItem) {
      //$ionicLoading.show();
      $scope.showLoading = true;
      var postData = { uid: uid, quiz_id: quiz_id };

      if (!value) {
        noTieBreak();
        return false
      }

      if ($scope.itemsTieBreak.type === "minor") {
        postData.minor_style = value;
        postData.major_style = localStorage.getItem('personaMajor');
      } else if ($scope.itemsTieBreak.type === "major") {
        postData.major_style = value;
        postData.minor_style = localStorage.getItem('personaMinor');
      }
      if ($scope.isMajorTieBreak) {
        $scope.modalLogin.hide();
      }

      $http.post("gitdiscern-user-quiz-tie-breaker-result/user-quiz-result.json", postData).then(function (response) {
        //                if ($rootScope.minorTieObject.length === 0 && !loadMinorItem) {
        //                    $scope.modalTieBreak.hide();
        //                }
        if ($scope.itemsTieBreak.type === "minor") {
          updateMinorResult({ highest_minor_style: value, minor_image: response.data.minor_style });
        } else if ($scope.itemsTieBreak.type === "major") {
          updateMajorResult({ highest_major_style: value, major_image: response.data.major_style })
        }

        if ($scope.isMajorTieBreak) {
          $scope.itemsTieBreak = { items: $scope.itemsMinorTieBreak, type: "minor" };
          $scope.quizTieBreakTxt = { header: "Nearly done! Last one", footer: "Submit" };
          $scope.isMajorTieBreak = false;

        } else {
          $scope.modalTieBreak.hide();
        }

        loadMinorItem = false;
        $ionicLoading.hide();

        if(!localStorage.getItem('quizPlayed')){
        analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Completed Once');
        }else{
       analytics.trackEvent('Style Quiz', "User: "+getUser(), 'Completed more than once');
        }

        localStorage.setItem('quizPlayed', '1');

        $scope.quizPlayed = true;
        discoverService.quizPlayed = true;


        // if submit from makeover submit makeover form too
        if ($state.current.name == 'app.design-makeover-styleQuiz' && localStorage.getItem('makeOverSubmited') == null && localStorage.getItem('personaMajor') != false && localStorage.getItem('personaMinor') != false) {
          $scope.$emit('submitFinalMakeoverData');
        }
        if ($state.current.name == 'app.discover') {
          quizService.updateQuizMsg();
          $scope.$emit('getStyleQproducts', localStorage.getItem('personaMinor'), localStorage.getItem('personaMajor'));
        }
        $rootScope.$emit('discoverLogin');

        $scope.showLoading = false;
      });

      $scope.closeQuiz();
    }

  };


  /**
   * @description : This function is used to checks persona to find major / minor
   * */
  var checkMajorMinor = function (value) {
    var result ;
    switch (parseInt(value)) {
      case 35:
      // Classic =
      case 40:
      // Modern =
      case 45:
        // Ethnic =
        result =true;
        break;
      default:
        result =false;
        break;
    }
    return result;
  };
  /**
   *@description: navActiveStatus function is for check avtive status in main navigation
   * */
  var navActiveStatus = function () {




    var myLocation = $location.path(),
      feed = '/app/feed',
      blog = '/app/blog',
      discover = '/app/discover',
      styleQuotient = '/app/your-style-quotient',
      looks = '/app/looks',
      pros = '/app/stylepro/' + (typeof localStorage.personaMajor !== "undefined" ? parseInt(localStorage.personaMajor) : ""),
      // pros = '/app/stylepro/' + (typeof localStorage.personaMajor !== "undefined" ? majorMinor[parseInt(localStorage.personaMajor)] : ""),
      splash = '/app/splash';



    switch (myLocation) {
      case feed:
        resetHeads();
        $scope.isFeedActive = true;
        $rootScope.hideMainMenu = true;
        break;
      case blog:
        resetHeads();
        $scope.isBlogActive = true;
        $rootScope.hideMainMenu = true;
        break;
      case discover:
        resetHeads();
        $scope.isDiscoverActive = true;
        $rootScope.hideMainMenu = true;
        discoverService.quizPlayed = localStorage.getItem("quizPlayed") == true;
        break;
      case styleQuotient:
        resetHeads();
        $scope.isDiscoverActive = true;
        $rootScope.hideMainMenu = true;
        discoverService.quizPlayed = localStorage.getItem("quizPlayed") == true;
        break;
      case looks:
        resetHeads();
        $scope.isLooksActive = true;
        $rootScope.hideMainMenu = true;
        break;
      //  case pros:
      //   resetHeads();
      //   $scope.isStyleproActive = true;
      //   $rootScope.hideMainMenu = true;
      //   break;
      case '/app/boutiques':
        resetHeads();
        $scope.isBoutiquesActive = true;
        $rootScope.hideMainMenu = true;
        break;
      case splash:
        $rootScope.hideMainMenu = true;
        break;
      default:
        $scope.isFeedActive = true;
        $scope.isNavHidden = true;
        $rootScope.hideMainMenu = false;

           var showNav = [feed,
                      blog,
                      discover,
                      styleQuotient,
                      looks,
                      //pros,
                      splash,
                      '/app/boutiques',
                      // '/app/boutique/detail',
                      '/app/offers',
                      //'/app/offers/detail'
                      ];

       if(angular.toJson(showNav).match(myLocation) == myLocation)  {
              resetHeads();
              $rootScope.hideMainMenu = true;
            }
    }

    function resetHeads() {
      $scope.isBlogActive = false;
      $scope.isFeedActive = false;
      $scope.isDiscoverActive = false;
      $scope.isLooksActive = false;
      $scope.isStyleproActive = false;
      $scope.isBoutiquesActive = false;
       $scope.isNavHidden = false;
    }


  };

  /**
   * @description : This function is used to move to a diff tab
   * */
  $scope.goToTab = function (link) {
    if (link === "app.stylepro") {
      // $state.go("app.stylepro", {personaMajor: majorMinor[parseInt(localStorage.personaMajor)]});
      $state.go("app.stylepro", { personaMajor: localStorage.personaMajor });
    }

  };

  /*
   * @description : This function is used to add back functionality in the app
   * */

  $scope.goBack = function () {
    $window.history.back();
    $rootScope.$emit('backButtonPressed');
    if ($rootScope.isNewMenuOpen) {
      $scope.hideSideMenu();
    }
  };


  // QUIZ MODAL ENDS

  var updateCartItems = function () {
    // CartService.addLocItemsToCart();
    CartService.getCartSummary();
  };

  /**
   * @description : This function is used to reset the scroll position to top on content load
   * */
  var resetScrollPosition = function () {
    angular.element(document).ready(function () {
      // $ionicScrollDelegate.scrollTo(0, 0, true);
    });
  };

  // Handle the elements on the global events here
  $rootScope.goToState = function (stateName, params) {
      if (!params) {
        $state.go(stateName);
      } else {
        $state.go(stateName, params);
      }
    }

  var bindEvents = function () {

    var pushNotification = $scope.$on('cloud:push:notification', function (event, data) {
      var msg = data.message;
      console.log(msg.title + ': ' + msg.text);
      console.log(data);
    });

    var updateQuizMsg = $scope.$on('updateQuizMsg', function () {
      quizService.updateQuizMsg();
    });

    var postQuizWithLogInInfo = $scope.$on('postQuizWithLogInInfo', function () {
      postQuizWithLogInInfo();
    });



    var goToState = $rootScope.$on('goToState', function (event, stateName) {
      $state.go(stateName);
    });


    $scope.$on("$destroy", pushNotification);
    $scope.$on("$destroy", updateQuizMsg);
    $scope.$on("$destroy", postQuizWithLogInInfo);
    $scope.$on("$destroy", goToState);


    interceptorFunction.recordFunction('bindEvents');
    var stateChangeStart = $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams, listingService) {
      $rootScope.toState = toState.name;
      /*  if(navigator.onLine == false && localStorage.getItem('hasConnection') == 1 ) {
          localStorage.setItem('hasConnection', 0);
           $state.go('app.no-internet');
         }

        if(typeof navigator.connection !== 'undefined'){

               var isOnline = $cordovaNetwork.isOnline();

               if (!isOnline) {
                   $rootScope.$emit('serviceMsg', "No Internet Connection");
                  // $rootScope.$emit('goToState', 'app.no-internet');
                    $state.go('app.no-internet');
                   return
               }
             }
             */

             switch (fromState.name) {
               case 'app.looks-landing':
                  $rootScope.$emit('removeCarousel');
                 break;
                 case 'app.looks-detail':
                  $rootScope.$emit('removeLookDetailCarousel');
                 break;
                  case 'app.builder-look-details':
                  $rootScope.$emit('removeBuilderLookDetailCarousel');
                 break;
             }

            switch (toState.name) {
               case 'app.looks-landing':
                  $rootScope.$emit('carouselCreated');
                 break;
                 case 'app.looks-detail':
                  $rootScope.$emit('carouselCreatedForLookDetail');
                 break;
                  case 'app.builder-look-details':
                  $rootScope.$emit('carouselCreatedBuilderLookDetail');
                 break;
             }




                if (fromState.name == 'app.address' && toState.name != 'app.payment') {
                  if(localStorage.getItem('couponCode')){
                  CartService.removeCoupon(localStorage.getItem('couponCode')).then(function(res){
                      if(res.data.output == 'success'){
                           console.log('coupon Removed');
                           CartService.getCartSummary();
                      }
                  });
                }
               }


      if (toState.name === "app.discover" && localStorage.getItem('quizPlayed')) {
        quizService.updateQuizMsg();
      }
      console.time(toState.name);
      $scope.$emit("hideMenu", false); // To show menu on state change
      if ((toState.name === "app.projects" || toState.name === "app.projects-visualizer" || toState.name === "app.projects-detail") && !$scope.isLoggedIn()) {
        event.preventDefault();
        $scope.modalLogin.show();
      } else {
        $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
      }
      if (fromState.name === "app.projects-visualizer") {
        $scope.$emit("resetHTML");
      }

      if (fromState.name !== "app.search-result-both".match("app.search")[0]) {

        // default fiilters for next page
        $rootScope.filterUrl = '&sort_by=commerce_price_amount&sort_order=ASC';
        // $rootScope.filterUrl = '&sort_by=created&sort_order=DESC';
      }

      if (fromState.name === "app.builder-look" || fromState.name === "app.builder-look-details" || fromState.name === "app.builder-product-details") {
        if ($rootScope.bldrQForm == true) {
          $rootScope.closeEnquirypopup();
        }
      }

      /*if( fromState.name === 'app.design-makeover-quizResult' || fromState.name === 'app.design-makeover-quizResult'){
       $state.go('app.feed');
       }*/

       if (fromState.name === "app.builder") {
        if ($rootScope.bldrQForm == true) {
          $rootScope.closeCodepopup();
        }
      }
      $rootScope.$emit("destroy");

      if (fromState.name === "app.product-detail" || fromState.name === "app.builder-look-details") {
        if ($rootScope.zestPopUpShow == true) {
          $rootScope.closePopUp();
        }
        if ($rootScope.payUPopUpShow == true) {
          $rootScope.closePayUPopUp();
        }
      }

      //$scope.styleproTitle = listingService.styleProfileDetails.name;

      if (toState.name == "app.no-internet")
        // if no internet hide loading
        $scope.$watch('$viewContentLoaded', function (nVal, oVal) {
          $ionicLoading.hide();
        });


      if (((typeof navigator.connection !== 'undefined' ? !navigator.connection : false) || !navigator.onLine) && fromState.name == "app.no-internet") {
        // if no internet hide loading
        $ionicLoading.hide();
      }

    });
    var stateChangeSuccess = $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
      console.timeEnd(toState.name);
      $scope.headTitle = toState.name.split("app.")[1].replace("-", " ");
      $rootScope.stateName = toState.name;
      $rootScope.stateParams = toParams;
      //todo : ideally they should be done using directive

      if (($scope.isQuizClosed && toState.name === "app.blog") || ($scope.isLoggedIn() && toState.name === "app.blog")) {

        $('ion-content').css('overflow', 'hidden');
        $rootScope.feedScroll = false;

      } else if (toState.name !== "app.blog") {

        $rootScope.feedScroll = true;
        $('ion-content').removeAttr('style');
      }

      if (toState.name === "app.hero-banner-detail") {
        $scope.headTitle = $stateParams.title || "Special Offers";
        $rootScope.searchResultTitle = $stateParams.title || "Special Offers";
      }


      if (toState.name === "app.projects") {
        $(".nav-bar-container").hide();
      } else if (toState.name === "app.projects-landing") {
        $scope.$emit("resetPage");
      }

      if (fromState.name === "app.projects") {
        $(".nav-bar-container").show();
      }

      if (toState.name === "app.looks") {
        $rootScope.$emit("updateLooks");
        $window.scrollTo(0, 0);
        $ionicScrollDelegate.scrollTo(0, 0, true);
      }

      if (toState.name === "app.model-says") {
        $scope.headTitle = "Blog";
      }


      if (fromState.name === "app.cart") {
        $scope.modalLogin.hide();
        CartService.itemsAvailable = [];
        /*  if (CartService.cart.length != 0) {
            CartService.placeOrder().then(function (response) {
              $ionicLoading.hide();
            }, function (response) {
              console.log(">>> This space needs to be handled better");
            });
          } */
      }

      if (toState.name === "app.design-makeover" || toState.name === "app.design-makeover-result" || toState.name === "app.builder" || toState.name === "app.builder-look" || toState.name === "app.builder-property" || toState.name === "app.builder-look-details" || toState.name === "app.builder-product-details" || toState.name === "app.builder-thankyou" || toState.name === "app.builder-design-service" || toState.name === "app.design-makeover-quizResult" || toState.name === "app.design-makeover-tag-your-photo" || toState.name === "app.design-makeover-details" || toState.name === "app.design-makeover-styleQuiz") {
        $rootScope.mkPop = true;
        $rootScope.showTopBar1 = false;
      } else {
        $rootScope.showTopBar1 = true;
      }


      if (fromState.name === "app.looks-landing") {
        $timeout(function(){
          $rootScope.$emit('makeGalleryopenedFalse');

        },1000);
      }
      if (toState.name === "app.looks-detail") {
        /* $scope.goBack = function () {
           $window.history.back(-1);
           $window.history.go(-1);
           $rootScope.$emit('backButtonPressed');
         };*/
         if ($rootScope.zestPopUpShow == true) {
           $rootScope.closePopUp();
         }
         if ($rootScope.payUPopUpShow == true) {
           $rootScope.closePayUPopUp();
         }
      }


      if (toState.name === "app.feed") {
        $(".super-top-bar").css("display", "inline-flex");
        // $(".bar-header.has-tabs-top").css("display", "inline-grid");
        $timeout(function () {
          $ionicSlideBoxDelegate.update();
        }, 50);

      }

      if (toState.name === "app.addaddress") {
        $scope.headTitle = "Add Address ";
      }
      // if (toState.name === "app.discover-category-list") {
      //     alert("hi");
      //     $('.has-subheader').css('top','60px');
      // }

      if (toState.name === "app.order-tracker") {
        $scope.headTitle = "My Order";
        $('.scroll-content').css('overflow-x', 'hidden');
      }

      if (toState.name === "app.address") {
        $(".has-header").css("top:54px;")
      }


      if ((toState.name === "app.search1")) {
        $rootScope.$emit('resetSearch');
        $rootScope.$emit('updateSearchHistory');

      }

      if ((toState.name === "app.search1") || toState.name == "app.search-result-both" || (toState.name === "app.search-result" && searchService.srchBox == 'show')) {
        $scope.isShowSearch = true;
      } else {
        $scope.isShowSearch = false;
      }
      // if (toState.name === "app.search-result") {
      //     $scope.headTitle = $rootScope.combine;
      // }
      if (toState.name === "app.about") {
        $scope.headTitle = "About Us";
      }

      if (toState.name === "app.termsofuse") {
        $scope.headTitle = "Terms Of Use";
      }

      if (toState.name === "app.privacypolicy") {
        $scope.headTitle = "Privacy Policy";
      }

      if (toState.name === "app.returnsandcancels") {
        $scope.headTitle = "Returns And Cancels";
      }

      if (toState.name === "app.discover" && !localStorage.getItem('quizPlayed') && !$scope.discoverPopShown) {
        if (!toParams.playQuizOutside || toParams.playQuizOutside != false){
          $scope.error = true;
          $scope.$emit('playQuizOutside', 1);
           $scope.discoverPopShown = true;
        }
      }

      if ((toState.name == "app.cart" || toState.name == "app.address" || toState.name == "app.payment" || toState.name == "app.thankyou") && (fromState.name != "app.cart" && fromState.name != "app.address" && fromState.name != "app.payment" && fromState.name != "app.thankyou" && fromState.name != "app.addaddress" && fromState.name != "app.editaddress" && fromState.name != "app.editBilladdress" && fromState.name != "app.billingAddress")) {
        $rootScope.continueShoping = "app.feed"; //fromState.name
        localStorage.removeItem('itemsAvailable');
      }

      // fo update search title
      if (toState.name == 'app.search-result-both' || toState.name == 'app.search-result-article' || toState.name == 'app.search-result-category' || toState.name == 'app.search-result-space' || toState.name == 'app.search-result-products') {
        $rootScope.$emit('updateHead', $rootScope.searchResultTitle + '(' + searchService.totalProd + ')');
        console.log("updated")
      }


      $scope.$watch('$viewContentLoaded', function (nVal, oVal) {
        $ionicLoading.hide();
        if (toState.name === "app.projects-visualizer") {
          $scope.$broadcast("checkEdit");
        }

        if (toState.name === "app.discover") {
          // data us per user status loged in or not
          $scope.isLoggedIn() == true ? $rootScope.$emit('discoverLogin') : $rootScope.$emit('discoverLogout')
          //$(".style").height($(window).height() - 80);
          //$(".nonLoggedIn .style").height($(window).height() - 80);
        }


        var userToken = localStorage.getItem('userToken');
        var userSessionId = localStorage.getItem('userSessionId');
        if (userSessionId == null && userToken == null) {
          refreshApp();
        }
        var firstLoad = true
        if (firstLoad) {

          window.requestAnimationFrame(function () {
            //  $ionicScrollDelegate.scrollBottom();
            firstLoad = false
          })
        }

      });
      resetScrollPosition(); //to reset scroll Position
      navActiveStatus(); // for main navigation

    });
    var stateChangeError = $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams) {
      $ionicLoading.hide();

    });

    //     $scope.disableVerticalScrolling = function() {
    //   $ionicScrollDelegate.getScrollView().options.scrollingY = false;
    // }
    //
    // $scope.enableVerticalScrolling = function() {
    //   $ionicScrollDelegate.getScrollView().options.scrollingY = true;
    // }
    /**
     * @description This function is used to update the head title
     * */
    var updateTitle = $rootScope.$on("updateHead", function (event, name) {
      $scope.headTitle = name;
    });
    var updatePersona = $rootScope.$on("updatePersona", function (event, info) {
      if (info) {
        updateMajorResult(info);
        updateMinorResult(info);
        $scope.quizPlayed = true
      } else {
        $scope.quizPlayed = false;
      }
    });
    /*var handleMenu = $rootScope.$on("hideMenu", function (event, hideMenu) {
     */
    /*todo : this is just a work around since ng-class was not working in mobile device*/
    /*
     if (!hideMenu) {
     $("#main-nav").removeClass("hide-menu");
     } else {
     $("#main-nav").addClass("hide-menu");
     }
     //            $scope.hideMenu = hideMenu;
     });*/

    // shoing login popup from out side
    var showloginFromOutside = $scope.$on("showLoginPopup", function () {
      $scope.modalLogin.show();

    });

    // Destroy all scope events on scope-destroy
    $scope.$on("$destroy", stateChangeStart);
    $scope.$on("$destroy", stateChangeSuccess);
    $scope.$on("$destroy", stateChangeError);
    $scope.$on("$destroy", updateTitle);
    $scope.$on("$destroy", updatePersona);
    //        $scope.$on("$destroy", handleMenu);

    $rootScope.$on('hardwareBackBtnPressed', function (event) {
      if ($scope.isQuizStarted == true) {
        $scope.closeQuiz();
        resetQuizDetailsForDiscover();
        $scope.isQuizStarted = false;
      }
      $ionicLoading.hide();
    });

    var playQuizbind = $scope.$on('playQuizOutside', function (event, stepNumber) {
      $scope.playQuiz(stepNumber);
    });

    $scope.$on("$destroy", playQuizbind);

    // show quize popup on first time app load
    // var quizAppLoad = localStorage.getItem("HTMLintroClosed")
    // var showQuizPopOnec = $scope.$watch(function() {
    //   return quizAppLoad
    //   }, function(nVal, oldVal) {
    //     if (quizAppLoad == 1) {
    //       initLoginNQuiz();
    //     }
    //   });
    //
    // $scope.$on("destroy", showQuizPopOnec);

  };



  var pageLoad = function () {
    interceptorFunction.recordFunction('pageLoad');
    bindEvents();
    init();
    navActiveStatus();
    updateCartItems();
    document.addEventListener("deviceready", function () {
      screen.lockOrientation('portrait');
       if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
       // StatusBar.hide();
       StatusBar.overlaysWebView(false);
      }

    }, false);
    $ionicLoading.hide();


  };



  /**
   * @description : This function is used to toggle between login/quiz popups
   * */
  var initLoginNQuiz = function () {
    /*if (!$scope.isLoggedIn()) {
     $timeout(function () {
     $scope.modalLogin.show();
     }, 1500);
     }*/
    if (localStorage.getItem('quizPlayed') == '' || localStorage.getItem('quizPlayed') == null) {
      $scope.modalQuiz.show();
      localStorage.setItem("quizShown", true);
      updateQuizTemplate('sofa');
    }
  };
  /**
   * @description : This function is used to get loggedIn status
   * */
  $scope.isLoggedIn = function () {
    return !(localStorage.getItem('userName') == '' || localStorage.getItem('userName') == null);
  };

  /**
   * @description : This function is used to convert an object(with numeric keys) to array
   * */
  function convertToArray(obj) {
    var arr = [];
    angular.forEach(obj, function (item) {
      arr.push(item);
    });
    return arr;
  }

  /**
   * This function is used to shuffle passed array
   * */
  function shuffle(array) {
    var currentIndex = array.length,
      temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  if (UtilityService.getDeviceInfo().id != '12233434') {

    $ionicPlatform.ready(function () {
      $cordovaGoogleAnalytics.debugMode();
      $cordovaGoogleAnalytics.startTrackerWithId('UA-69476693-2');

      var user = localStorage.getItem('userUid') || UtilityService.getDeviceInfo().id
      analytics.setUserId(user);
      $cordovaGoogleAnalytics.setUserId(user);

      $cordovaGoogleAnalytics.trackException('Description', Fatal);
    });
  }

  var Fatal = function (err) {
    console.log(err);
  }
  /**
   * @discription: checking makeOver status in diffrent Scenarios
   * from: passing agument to check if user coming via clicking on menu(fromMenu) button
   *   or caling function after he is done first step of make over and app is sking to login first(loginFirst)
   * */
  var checkMkOverStatus = function (from) {

    makeOverService.getUserDesignEntry().then(function () {
      if (!makeOverService.firstLookExist && from == 'fromMenu') {
        // if user coming first time
        $state.go('app.design-makeover')
      } else if (makeOverService.firstLookExist) {
        // if user is not login and reached to step one and login with old ID where he already submit the form
        from == 'loginFirst' ? $rootScope.$emit("serviceMsg", "Look already Submited") : console.log('Look already Submited')
        // if user coming after submit the form
        $state.go('app.design-makeover-result');

      }
    });

  };
  var refreshApp = function () {
    $scope.logOut();
    CartService.getCartSummary();
    $state.go('app.feed');
  };

  var pushRegister = function () {
    if(typeof PushNotification !== 'undefined') {

    var push = PushNotification.init({
                android: {
                  senderID: "362185478895",
                  'content-available':1,
                  forceShow:true,
                   icon: "icon" ,
                   iconColor: "#e74227"
                },
                  browser: {
                      pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                  },
                ios: {
                  senderID:'362185478895',
                  alert: true,
                  badge: true,
                  sound: true,
                  gcmSandbox:false,
                  "content_available": 1
                },
                windows: {}
              });


      push.on('registration', function (deviceToken) {
        console.log(deviceToken);
        var token = { token: deviceToken.registrationId, registered: true, saved: false }
        $rootScope.deviceToken = deviceToken.registrationId || ''; // Send this devicetoken to your app servers
        //return $ionicPush.saveToken(token, { ignore_user: $scope.isLoggedIn() });
         UtilityService.addTopic('All-Devices'); // topic created for all users
      });

      push.on('error', function (data) {
        console.log("Push plugin registration error" + JSON.stringify(data));
      });

      push.on('notification', function (data) {
        console.log(data);
        if(UtilityService.getDeviceInfo().platform == "iOS" && data.additionalData.foreground){
          // show popup is user on makeover page

            var title = data.title || "Notification",
            body =  data.message || ''

            var nitifyPopup = $ionicPopup.confirm({
                title: "Notification",
                template: title+": "+body,
                cancelType: 'button-light',
                cssClass: 'mkovrPopup'
            });
            nitifyPopup.then(function(res) {
                    if (res) {
                    deepLink(data.additionalData.click_action);
                } else {
                    console.log('You are not sure');
                }
            });


            //alert(data.title +" :" + data.message);
        }else if(UtilityService.getDeviceInfo().platform == "iOS" && !data.additionalData.foreground){

             deepLink(data.additionalData.click_action);
            push.finish(function() {
              console.log("processing of push data is finished");
            }, function() {
              console.log("something went wrong with push.finish for ID = " + data.additionalData.notId)
            }, data.additionalData.notId);

        }

         if(data.additionalData.click_action && UtilityService.getDeviceInfo().platform != "iOS" ){ //&& !data.additionalData.foreground
          // $state.go("app."+data.additionalData.click_action);
           deepLink(data.additionalData.click_action);
         }
        $rootScope.$broadcast('push-notification-data', { data: data.additionalData });
      });
    }
  }



   $rootScope.getInfo = function(){
      function setHeader(xhr) {
         xhr.setRequestHeader('Authorization', 'key=AAAAVFPv0u8:APA91bHymuDb5W027Gl68tLyi9_WkZcFvoI2v3lR9QXlFEX0ZXd3Z44crM-Xc8XmNKVFxn0l1_SAgjqVpsSj0SbjALJNRUoYbm3yb_4XIdqUHYYmqd0_W9MEGOqGuLi2xLBXA2O_AfE6');
         xhr.setRequestHeader('Content-Type', 'application/json');
      }

    $.ajax({
        url: 'https://iid.googleapis.com/iid/info/'+$rootScope.deviceToken+"?details=true",
        type: 'GET',
        datatype: 'json',
        success: function(data) {  console.log(data);},
        error: function(data) { console.log(data)},
        beforeSend: setHeader
    });
  }
    /**
     * @description: this function is used to redirect app to perticular page
     * @param {stateName} stateName  is the locatoin of redirection , its could be a state name with pramas or only state name
     */
    var deepLink = function(stateName){
              if(stateName.indexOf("?") != -1){
                 var state = stateName.split(/(\?)/)[0],
                      prams = stateName.split(/(\?)/)[2],
                      prams = prams.replace(/(\=)/g, '":"'),
                      prams = '{"'+prams.replace(/(\&)/g, '","')+'"}';
                      prams = JSON.parse(prams)
                      console.log("referrer: "+prams);
                      $state.go("app."+state, prams);
                }else{
                      $state.go("app."+stateName);
                    }
    }

  // window.handleOpenURL(url) {
  //       // This function is triggered by the LaunchMyApp Plugin
  //       setTimeout(function() {
  //       console.log(url);
  //       deepLink(url);
  //   }, 400);
  //   }



  document.addEventListener('deviceready', function () {
    console.log('ready');


    //  debugger;
   // UtilityService.getDeviceInfo().platform == "iOS" ? iosRegister() : registerMe()
   // UtilityService.push();
   // console.log( "register didShow received!" );

    // var listener = function( e ) {
    //   //log: didShow received! userInfo: {"data":"test"}
    //   console.log( "didShow received! userInfo: " + JSON.stringify(e)  );
    // }
    // window.broadcaster.addEventListener( "didShow", listener);
if(UtilityService.getDeviceInfo().platform == "iOS"){
    GappTrack.track("855099745", "8FcgCMDminIQ4ZLflwM", "0.00", "NO");
}
  pushRegister();

  //console.log('registered');


  }, false);

//  var cartTopicSubscribed = false;


//  $rootScope.cartTopicSubscribe = function(){

//      if(CartService.cart.totalPrice >= 20000 && !cartTopicSubscribed){
//           UtilityService.addTopic("Cart-20000");
//           cartTopicSubscribed = true;
//         }else if(CartService.cart.totalPrice < 20000 && cartTopicSubscribed){
//           UtilityService.removeTopic("Cart-20000");
//           cartTopicSubscribed = false;
//       }
//   }








  var init = function () {

    var platform = UtilityService.getDeviceInfo().platform == "iOS" ? 'platform-ios' : 'platform-android';
    angular.element('body').addClass(platform);
    // getting Market App Version 
     
    apkDownloadLink = UtilityService.getDeviceInfo().platform == "iOS" ? appleDownloadLink : apkDownloadLink;
    clearSelectedQuizData();
    $scope.personaUrl = "/sites/default/files/styles/medium_style_pro/public/";
    $scope.loginForm = {};
    $rootScope.isClickEnable = true;
    $rootScope.combine = '';
    $rootScope.bldrQForm = false;
    $rootScope.showTopBar1 = true;
    $rootScope.mkPop = true; // for makerover
    $rootScope.makeOverSubmited = false;
    interceptorFunction.recordFunction('pageInit');
    localStorage.getItem("introShown") == 1 ? $(".view-container").show() : $(".view-container").hide();
    $rootScope.slideHeaderPrevious = 0;
    $rootScope.stateName = 'discover';
    $scope.isProjectSelected = false;
    $scope.selectedQuizData.looks = [];
    $rootScope.minorTieObject = [];
    $rootScope.tieBreakSelected = 0;
    $scope.beforeQuiz = false;
    $scope.isQuizStarted = false;
    $scope.quizStatus = { name: "sofa", header: "sofa-icon-head", footer: "sofa-icon-footer", footerText: "NEXT" };
    $scope.quizTieBreakTxt = { header: "Nearly done! Last one", footer: "Submit" };
    $scope.currentSectionName = "sofa";
    $scope.loginTab = 1;
    $scope.isLoggedIn();
    $scope.isNavHidden = false;
    $rootScope.stateName = $state.current.name;
    // Form data for the login modal
    $scope.loginData = {};
    // Following variables are used in menu
    $scope.name = localStorage.getItem("userName");
    $scope.hidePsw = false;
    $scope.resetEmail = {};
    $scope.isMajorTieBreak = false;

    $rootScope.cartCount = 0;
    $rootScope.hideMainMenu = true;
    $scope.introShown = false;
    $rootScope.profilePic = localStorage.getItem("userProfilePic") === null ? "img/dp2.png" : localStorage.getItem("userProfilePic");
    $rootScope.majorResultText = localStorage.getItem("majorResultText");
    $rootScope.majorPersonaImage = localStorage.getItem("majorPersonaImage");
    $rootScope.minorResultText = localStorage.getItem("minorResultText");
    $rootScope.minorPersonaImage = localStorage.getItem("minorPersonaImage");
    $rootScope.userEmail = localStorage.getItem("userEmail") !== null ? localStorage.getItem("userEmail") : "";
    $rootScope.userPicture = (localStorage.getItem("userPicture") !== null && localStorage.getItem("userPicture") !== "null") ? localStorage.getItem("userPicture") : "img/d-logo-quiz.png";
    $rootScope.feedShareImage = "";
    localStorage.setItem("quizCount", 0);

    // default fiilters for next page
    $rootScope.filterUrl = '&sort_by=commerce_price_amount&sort_order=ASC';
    //$rootScope.filterUrl = '&sort_by=created&sort_order=DESC'; // for next page fiilters

    $rootScope.isBuildersMenuActive = false
    // shows intro carousel at app load
    $timeout(function () {
      $scope.closeIntro();
      $scope.introShown = true;


    }, 10);
    $rootScope.spaceSelector = { isSpaceSelectorActive: false, spaceId: 0 };

        apkDownloadLink =  appleDownloadLink = getShareUrl();

        /**
         * @description: this code is used to get referrer string if user install the app from referer Link
         * Google refferer link example: intent://scan/#Intent;scheme=discern://product-detail?product_id=7437;package=com.discernliving.app;S.market_referrer=product-detail?product_id=7437;end
         */

          if(UtilityService.getDeviceInfo().id != "12233434"){

             $ionicPlatform.ready(function () {

             

              var prefs = plugins.appPreferences;

                   $timeout(function(){
                          /**
                           * @description: this function is used to get referrer string
                           */
                          prefs.fetch('referrer').then(function(ref){
                          if(ref){
                            //console.log('success: ref');
                            deepLink(ref);
                          /**
                           * @description: this function is used to remove referrer string after use it successfully
                           */
                            prefs.remove(function(){
                                // console.log('success: remove ref');
                            }, function(){
                               // console.log('error: remove ref');
                            }, 'referrer')

                          }
                        }, function  (error) {
                          //console.log("getting referrer err: "+ error);
                        });

                   }, 30);

             });
          }


          console.timeEnd("appStart");


  };
  pageLoad();
  quizService.updateQuizMsg();
  $scope.quizPlayed = discoverService.quizPlayed = localStorage.getItem("quizPlayed") == true;
});
