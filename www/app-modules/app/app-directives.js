/**
 * @description This directive is used to add an overlay screen on the WIP content
 * */
appModuleController
  .directive("overlayWip", function() {
    return {
      restrict: "A",
      link: function(scope, $element, attr) {
        /**
         * @description This function is used to bind all the events of this directive
         * */
        var bindEvents = function(e) {
          $element.bind("scroll", function(e) {
            e.preventDefault();
            $element.scrollTop(0);
          });
        };
        /**
         * @description This function is used to initialize the overlay cont
         * */
        var pageInit = function() {
          $(function() {
            appendOverlayCont();
            bindEvents();
          });
        };
        /**
         * @description This function is used to create overlay element
         * */
        var appendOverlayCont = function() {
          $element.append("<div class='overlay-wip'></div>");
          $element.find(".overlay-wip").height($element.height());
        };
        pageInit();
      }
    };
  })
  .directive("appendLoadingDiscern", ['$timeout', '$ionicSlideBoxDelegate', function($timeout, $ionicSlideBoxDelegate) {
    return {
      restrict: "A",
      link: function(scope, $element, attr) {
        //var getSVG = loadingSvg
        var loader = $("<div class='image-preloader'>" + getSVG() + "</div>");// $("<div class='discern-loader'>" + getSVG() + "</div>");
        $element.after(loader);
        // var timout = $timeout(function() {
        //   loader.remove();
        // }, 10000);
        // scope.$on("destroy", timout);
        $element.bind("load", function(e) {
          loader.remove();
          $element.fadeIn(); 
          $ionicSlideBoxDelegate.update()
        });
      }
    }
  }])
  .directive("scrollWatch", ['$rootScope', function($rootScope) {
    return function(scope, elem, attr) {
      var start = 0;
      var threshold = 150;

      elem.bind('scroll', function(e) {
        $rootScope.slideHeader = (e.originalEvent.detail.scrollTop - start > threshold);
        if ($rootScope.slideHeaderPrevious >= e.originalEvent.detail.scrollTop - start) {
          $rootScope.slideHeader = false;
        }

        $rootScope.slideHeaderPrevious = e.originalEvent.detail.scrollTop - start;
        $rootScope.$apply();
      });
    };
  }]).
directive('looksHighlighter', ['$rootScope', 'looksService', function($rootScope, looksService) {
  return function(scope, elem, attr) {

    looksService.setScrollElement($(elem).parents(".looks-content"));

    var documentWindow = {
      getHeight: function() {
        return window.innerHeight;
      },
      getContentHeight: function() {
        return this.getHeight() - 50;
      }
    };

    var looksHighlighter = {

      getStrongHighlight: function() {
        return documentWindow.getContentHeight() / 2;
      },
      getMediumHightlight: function() {
        return (this.getStrongHighlight() / 2) + (this.getStrongHighlight() / 8);
      }
    }



    var init = function() {
      $(elem).css({
        'width': '100%',
        'height': looksHighlighter.getMediumHightlight() + "px"
      });
      if (scope.$first) {
        $(elem).css({
          'width': '100%',
          'height': looksHighlighter.getStrongHighlight() + "px"
        });
      }
      var styleQuotient = $(elem).parent().find('.styleQuotient');

      styleQuotient.css('top', looksHighlighter.getStrongHighlight() - ((styleQuotient.css('height').split("px")[0]) / 2));
    }


    init();

    window.addEventListener("resize", init);



    //console.log(documentWindow.getHeight()+":"+looksHighlighter.getStrongHighlight())


    //console.log($(elem).css('height'));
    // $(elem:first-child).css('height',looksHighlighter.getMediumHightlight()+"px");
  }

}]).
directive("modalPopup", function() {
  return {
    template: ' <div class="popBackground"  data-ng-click="closePopup();"></div>' +
      '<div id="popup1" class="modal-box" >' +
      '<header>' +
      '<a href="javascript:" class="close" data-ng-click="closePopup();">×</a>' +
      '</header>' +
      ' <div class="modal-body">' +
      '<h3>{{message}}</h3>' +
      '</div>' +
      ' </div>',
    restrict: "A",
    scope: {
      message: '@',
      closePopup: '&'
    }

  }
}).directive('moreLess', function() {
  return {
    transclude: true,
    scope: {
      more: '=',
      less: '=',
    },
    template: "<ng-transclude class='more-less'></ng-transclude><div class='footer-buttons'><a id='more_btn'>+<span>{{more}}</span></a><a id='less_btn'>-<span>{{less}}</span></a></div>",
    link: function(scope, elem, attrs) {
      var morelessInfo = $(elem).find('.more-less-info');
      morelessInfo.addClass('less-info');
      var moreBtn = $(elem).find('#more_btn');
      var lessBtn = $(elem).find('#less_btn');
      moreBtn.show();
      lessBtn.hide();
      moreBtn.click(function() {
        morelessInfo.removeClass('less-info').addClass('more-info');
        moreBtn.delay(50).hide(0);
        lessBtn.delay(50).show(0);
      });
      lessBtn.click(function() {
        morelessInfo.addClass('less-info').removeClass('more-info');
        moreBtn.delay(100).show(0);
        lessBtn.delay(100).hide(0);
      });
    }
  }
}).directive('showHide', ['$ionicScrollDelegate', '$rootScope', function($ionicScrollDelegate, $rootScope) {
  /**
   *@description:This Directive is used show/hide text
   * */
  return {
    transclude: true,
    restrict: 'A',
    scope: {
      proDetails: '=',
      fullTxt: '=',
      textLimit: '@',
      txtClass: '@',
      otherContent: '='
    },
    template: '<p ng-class="{{txtClass}}"></p> <div ng-show="collapsed" ng-include="otherContent"></div> <div class="more-wrap" ng-if="fullTxt.length > textLimit" delegate-handle="moreBtn" > <a href ng-click="showMore2();collapsed = !collapsed;" ng-hide="collapsed" class="more">+ MORE</a> <a href ng-click="showLess2(); collapsed = !collapsed;" class="more" ng-show="collapsed"> - LESS</a> </div>',
    link: function(scope, elem, attrs) {


      if (scope.fullTxt) {
        var thetemp = scope.otherContent;
        var fullTxt2 = scope.fullTxt.replace(/\s\s+/g, ' ');
            //fullTxt2 = fullTxt2.replace(/<\/?[^>]+(>|$)/g, ""),
       if (fullTxt2.length > 150){
         var  less = fullTxt2.substr(0, scope.textLimit), // less text with last uncompleted word
          lessTxtFormated = fullTxt2.substr(0, less.lastIndexOf(" ")), // removed last uncompleted word
          moreTxt = fullTxt2.split(lessTxtFormated)[1]; // rest of text
       }else{
          var  lessTxtFormated = fullTxt2
      }


        $(elem).children('p').text(lessTxtFormated)
        scope.showMore2 = function() {
          $(elem).children('p').text(lessTxtFormated + moreTxt);
          $ionicScrollDelegate.resize();
        }

        scope.showLess2 = function() {
          $(elem).children('p').text(lessTxtFormated);
          $ionicScrollDelegate.resize();
        }
      }
    }
  }

}]).directive('floatIcon', ['$rootScope', '$state', '$window', function($rootScope, $state, $window) {
  /**
   *@description:This Directive is used show/hide text
   * */
  return {
      transclude: true,
    restrict: 'A',
    scope: { 
      btnTxt: '@',
    },
    link: function(scope, elem, attrs) {

      var displayIcon = function() {
        var getWinH = $(window).height();
        $(elem).css({
          top: getWinH
        }).attr('id', 'floating-icon').html(scope.btnTxt);
      }
      displayIcon();
      angular.element(window).on('resize', function() {
        displayIcon();
      });
    }}

}]).directive('productSwipe', function() {

  return {
    restrict: 'EA',
    scope: {
      slides: '=',
      options: '=',
      galleryopened: '=',
      nextSlide: '&',
      closeGallery: '&'

    },
    template: '<div class="product-swipe" ng-photoswipe="product-swipe"  slides="slides" galleryopened="galleryopened" slide-selector=".slide-list > li > img"  on-close="closeGallery()" next-slide="nextSlide({index:index})"   options="options"></div> '
  }
}).directive('numericRestrict', function() {
  return {
    restrict: "A",
    link: function(scope, elem, attrs) {
      elem.bind('keypress keydown keyup', function(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57));

      });

    }
  }
}).directive('ionAffix', ['$ionicPosition', '$compile', function($ionicPosition, $compile) {
  // this directive is used to make elements sticky
  // keeping the Ionic specific stuff separated so that they can be changed and used within an other context

  // see https://api.jquery.com/closest/ and http://ionicframework.com/docs/api/utility/ionic.DomUtil/
  function getParentWithClass(elementSelector, parentClass) {
    return angular.element(ionic.DomUtil.getParentWithClass(elementSelector[0], parentClass));
  }

  // see http://underscorejs.org/#throttle
  function throttle(theFunction) {
    return ionic.Utils.throttle(theFunction);
  }

  // see http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
  // see http://ionicframework.com/docs/api/utility/ionic.DomUtil/
  function requestAnimationFrame(callback) {
    return ionic.requestAnimationFrame(callback);
  }

  // see https://api.jquery.com/offset/
  // see http://ionicframework.com/docs/api/service/$ionicPosition/
  function offset(elementSelector) {
    return $ionicPosition.offset(elementSelector);
  }

  // see https://api.jquery.com/position/
  // see http://ionicframework.com/docs/api/service/$ionicPosition/
  function position(elementSelector) {
    return $ionicPosition.position(elementSelector);
  }

  function applyTransform(element, transformString) {
    // do not apply the transformation if it is already applied
    if (element.style[ionic.CSS.TRANSFORM] == transformString) {} else {
      element.style[ionic.CSS.TRANSFORM] = transformString;
    }
  }

  function translateUp(element, dy, executeImmediately) {
    var translateDyPixelsUp = dy == 0 ? 'translate3d(0px, 0px, 0px)' : 'translate3d(0px, -' + dy + 'px, 0px)';
    // if immediate execution is requested, then just execute immediately
    // if not, execute in the animation frame.
    if (executeImmediately) {
      applyTransform(element, translateDyPixelsUp);
    } else {
      // see http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
      // see http://ionicframework.com/docs/api/utility/ionic.DomUtil/
      requestAnimationFrame(function() {
        applyTransform(element, translateDyPixelsUp);
      });
    }
  }

  var CALCULATION_THROTTLE_MS = 500;

  return {
    // only allow adding this directive to elements as an attribute
    restrict: 'A',
    // we need $ionicScroll for adding the clone of affix element to the scroll container
    // $ionicScroll.element gives us that
    require: '^$ionicScroll',
    link: function($scope, $element, $attr, $ionicScroll) {
      // get the affix's container. element will be affix for that container.
      // affix's container will be matched by "affix-within-parent-with-class" attribute.
      // if it is not provided, parent element will be assumed as the container
      var $container;
      if ($attr.affixWithinParentWithClass) {
        $container = getParentWithClass($element, $attr.affixWithinParentWithClass);
        if (!$container) {
          $container = $element.parent();
        }
      } else {
        $container = $element.parent();
      }

      var scrollMin = 0;
      var scrollMax = 0;
      var scrollTransition = 0;
      // calculate the scroll limits for the affix element and the affix's container
      var calculateScrollLimits = function(scrollTop) {
        var containerPosition = position($container);
        var elementOffset = offset($element);

        var containerTop = containerPosition.top;
        var containerHeight = containerPosition.height;

        var affixHeight = elementOffset.height;

        scrollMin = scrollTop + containerTop;
        scrollMax = scrollMin + containerHeight;
        scrollTransition = scrollMax - affixHeight;
      };
      // throttled version of the same calculation
      var throttledCalculateScrollLimits = throttle(
        calculateScrollLimits,
        CALCULATION_THROTTLE_MS, {
          trailing: false
        }
      );

      var affixClone = null;

      // creates the affix clone and adds it to DOM. by default it is put to top
      var createAffixClone = function() {
        var clone = $element.clone().css({
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0
        });

        // if directive is given an additional CSS class to apply to the clone, then apply it
        if ($attr.affixClass) {
          clone.addClass($attr.affixClass);
        }

        // remove the directive matching attribute from the clone, so that an affix is not created for the clone as well.
        clone.removeAttr('ion-affix').removeAttr('data-ion-affix').removeAttr('x-ion-affix');

        angular.element($ionicScroll.element).append(clone);

        // compile the clone so that anything in it is in Angular lifecycle.
        $compile(clone)($scope);

        return clone;
      };

      // removes the affix clone from DOM. also deletes the reference to it in the memory.
      var removeAffixClone = function() {
        if (affixClone)
          affixClone.remove();
        affixClone = null;
      };

      $scope.$on("$destroy", function() {
        // 2 important things on destroy:
        // remove the clone
        // unbind the scroll listener
        // see https://github.com/aliok/ion-affix/issues/1
        removeAffixClone();
        angular.element($ionicScroll.element).off('scroll');
      });


      angular.element($ionicScroll.element).on('scroll', function(event) {
        var scrollTop = (event.detail || event.originalEvent && event.originalEvent.detail).scrollTop;
        // when scroll to top, we should always execute the immediate calculation.
        // this is because of some weird problem which is hard to describe.
        // if you want to experiment, always use the throttled one and just click on the page
        // you will see all affix elements stacked on top
        if (scrollTop == 0) {
          calculateScrollLimits(scrollTop);
        } else {
          throttledCalculateScrollLimits(scrollTop);
        }

        // when we scrolled to the container, create the clone of element and place it on top
        if (scrollTop >= scrollMin && scrollTop <= scrollMax) {

          // we need to track if we created the clone just now
          // that is important since normally we apply the transforms in the animation frame
          // but, we need to apply the transform immediately when we add the element for the first time. otherwise it is too late!
          var cloneCreatedJustNow = false;
          if (!affixClone) {
            affixClone = createAffixClone();
            cloneCreatedJustNow = true;
          }

          // if we're reaching towards the end of the container, apply some nice translation to move up/down the clone
          // but if we're reached already to the container and we're far away than the end, move clone to top
          if (scrollTop > scrollTransition) {
            translateUp(affixClone[0], Math.floor(scrollTop - scrollTransition), cloneCreatedJustNow);
          } else {
            translateUp(affixClone[0], 0, cloneCreatedJustNow);
          }
        } else {
          removeAffixClone();
        }
      });
    }
  }
}]);
