/**
 * @ngdoc interceptor : This is the interceptor to intercept all the BE calls req/res
 * @name HttpInterceptor
 * */
appModule.factory("HttpInterceptor", ['$q', '$rootScope', '$timeout', '$ionicPlatform', '$cordovaNetwork',  function($q, $rootScope, $timeout, $ionicPlatform, $cordovaNetwork) {

  var requestedUrl = {},
       isErrorShown = false;
  return {
    request: function(config) {
      // check if internet available
      // code to exit app:  ionic.Platform.exitApp();
      if (((typeof navigator.connection !== 'undefined' ? !navigator.connection : false) || !navigator.onLine) && config.url.indexOf("html") == -1) {
        if (!isErrorShown) {
          $rootScope.$emit('goToState', 'app.no-internet');
          $(".page-load-loader").remove();
          isErrorShown = true;
        }
        return $q.reject();
      }
      isErrorShown = false;

      var token = localStorage.getItem("userToken"),
        deferred = $q.defer();
        if ((!new RegExp(/app-modules/g).test(config.url) || !new RegExp(/modules/g).test(config.url)) && config.url !== "proProjects" && config.url !== "enquiryForm" && config.url !== "looksList"  && config.url !== "looksDetailPop" && config.url !== "projectDetailModal" && config.url !== "enquiryFormPros" && config.url !== "projectModal" && config.url !== "productDetailGalleryModal") {

        config.url = config.url.indexOf("http") == -1 ? LIVE_URL + config.url : config.url;
        //config.url = config.url !== url.payU ? LIVE_URL + config.url : config.url;
        requestedUrl[config.url] = new Date().getTime();
      }

      /**
      * @description: this function is used to defind if requested url is for cart or login/logOut send {Token} in header
      **/
      var isTokenNeeded = function() {
        var tokenURLs = angular.copy(url);
        tokenURLs = angular.extend(tokenURLs, login_urls, url.ZEST_ORDER_STATUS);
        return JSON.stringify(tokenURLs).match(config.url.replace(LIVE_URL, '').split('?')[0]) == config.url.replace(LIVE_URL, '').split('?')[0]
      }

      /**
       * @description: adding headers for cart and login services
       */
      if (token !== null && token.length && isTokenNeeded()) {
        config.headers["x-csrf-token"] = token;
        config.headers["Cache-Control"] = 'no-cache';
      }else if(config.method == 'POST' && localStorage.getItem('userUid') != null){
        config.headers["x-csrf-token"] = token;
      }


      if(!globle_cache){
        //config.headers["Cache-Control"] = 'no-cache';
      }

      if (login_urls.user_logout == config.url.replace(LIVE_URL, '')) {
        config.headers["Cookie"] = localStorage.getItem('userSessionName') + "=" + localStorage.getItem('userSessionId') + ";"
        //config.withCredentials = false
      }


     if(url.getCart == config.url.replace(LIVE_URL, '') && localStorage.getItem("userEmail") != null ) {
        if(window.cookieEmperor) window.cookieEmperor.setCookie(LIVE_URL, localStorage.getItem('userSessionName'),  localStorage.getItem('userSessionId'),
        function() { 
            //$state.go(backToState); 
            console.log('A cookie has been set');
          },
          function(error) {
            console.log('Error setting cookie: '+error);
           // $state.go('app.feed');
        });

      }

      return config || $q.when(config);
    },
    response: function(response) {
      delete requestedUrl[response.config.url];
      return response || $q.when(response);
    },
    responseError: function(rejection) {
      return $q.reject(rejection);
    }
  }

}]);
