/*
 * todo : why is everything inside menuContent ?
 * */
appModule
  .run(function($ionicPlatform, $ionicPush, $location, $rootScope) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
      //Google analytics start
      if(typeof analytics !== "undefined"){
         //analytics.startTrackerWithId('UA-77476693-2'); // remove
        analytics.startTrackerWithId('UA-69476693-2'); // for analytics.discern@gmail.com
        //analytics.startTrackerWithId('UA-93712257-1'); // for contact@discernliving.com
        //analytics.trackView('App Load');


        console.log('starting analytics')
      }else{
        console.log('analytics could not be loaded')
      }
      //Google analytics end


      // $location.path('/app/feed');
      // $rootScope.$apply();

//       $ionicPush.register().then(function(t) {
//   return $ionicPush.saveToken(t);
// }).then(function(t) {
//   console.log('Run-Token saved:', t.token);
// });

    });
  })
  .config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider, $ionicCloudProvider) {
      $ionicConfigProvider.views.swipeBackEnabled(false);
      $httpProvider.interceptors.push('HttpInterceptor');
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
      $httpProvider.defaults.useXDomain = true;
      $httpProvider.defaults.withCredentials = false;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
       
    $stateProvider
      .state('app', {
        url: "/app",
        cache: false,
        abstract: true,
        templateUrl: "app-modules/menu/menu.html",
        controller: 'AppCtrl'
      }).state('app.home', {
        url:"/app/home",
         views: {
          'menuContent': {
            templateUrl: "app-modules/feed/home.html",
            controller: "homeCtrl",
          }
        }
    })
      .state('app.about', {
        url: "/about",
        views: {
          'menuContent': {
            templateUrl: "app-modules/static/about.html",
            controller: "StaticCtrl",
            resolve: {
              staticTemplate: ['StaticService', function(staticService) {
                // return staticService.getTemplate(urlStatic.about).then(function(response) {
                //   staticService.template = response[0].Body;
                //   staticService.logoimg = response[0].header_image;
                // })
              }]
            }
          }
        }
      })
      .state('app.faq', {
        url: "/faq",
        views: {
          'menuContent': {
            templateUrl: "app-modules/static/static.html",
            controller: "StaticCtrl",
            resolve: {
              staticTemplate: ['StaticService', function(staticService) {
                // return staticService.getTemplate(urlStatic.faq).then(function(response) {
                //   staticService.template = response[0].Body;
                //   staticService.logoimg = response[0].header_image;
                // });
              }]
            }
          }
        }
      })
      .state('app.termsofuse', {
        url: "/termsofuse",
        views: {
          'menuContent': {
            templateUrl: "app-modules/static/static.html",
            controller: "StaticCtrl",
            resolve: { //
              staticTemplate: ['StaticService', function(staticService) {
                // return staticService.getTemplate(urlStatic.termsofuse).then(function(response) {
                //   staticService.template = response[0].Body;
                //   staticService.logoimg = response[0].header_image;
                // });
              }]
            }
          }
        }
      })
      .state('app.privacypolicy', {
        url: "/privacypolicy",
        views: {
          'menuContent': {
            templateUrl: "app-modules/static/static.html",
            controller: "StaticCtrl",
            resolve: { //
              staticTemplate: ['StaticService', function(staticService) {
                return staticService.getTemplate(urlStatic.privacypolicy).then(function(response) {
                  staticService.template = response[0].Body;
                  staticService.logoimg = response[0].header_image;
                });
              }]
            }
          }
        }
      })
      .state('app.returnsandcancels', {
        url: "/returnsandcancels",
        views: {
          'menuContent': {
            templateUrl: "app-modules/static/static.html",
            controller: "StaticCtrl",
            resolve: { //
              staticTemplate: ['StaticService', function(staticService) {
                return staticService.getTemplate(urlStatic.returnsandcancels).then(function(response) {
                  staticService.template = response[0].Body;
                });
              }]
            }
          }
        }
      })
      .state('app.cart', {
        url: "/cart",
        cache: false,
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/cart.html",
            controller: "CartCtrl",
            resolve: {
              cartSummary: ['CartService', function(cartService) {
                //return cartService.getCartSummary()
              }],
              removeCoupon: ['CartService', function(cartService){
                if(cartService.cart.length && cartService.cart[0].coupon_labels.length){
                  // return cartService.removeCoupon(cartService.cart[0].coupon_labels[0])
                }

              }]
            }
          }
        }
      })
      .state('app.address', {
        url: "/address",
        cache: false,
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/address/address.html",
            controller: "addressCtrl",
            resolve: {
              alladdress: ['CartService', function(cartService) {
               // return cartService.savedAddress()
              }],
              showBillAddress: ['CartService', function(cartService) {
               //return cartService.savedBillAddress()
              }]/*,
              cartSummary: ['CartService', function(cartService) {
                return cartService.getCartSummary()
              }]*/
            }
          }
        }
      }).state('app.addaddress', {
        url: "/addaddress?status",
        cache: false,
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/address/addAddress.html",
            controller: "addAddressCtrl"
          }
        }
      })
      .state('app.editaddress', {
        url: "/addaddress?profileId&status",
        cache: false,
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/address/addAddress.html",
            controller: "editAddressCtrl"
          }
        }
      }).state('app.editBilladdress', {
        url: "/addaddress?profileId",
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/address/addAddress.html",
            controller: "editBillAddressCtrl"
          }
        }
      }).state('app.billingAddress', {
        url: "/billingAddress",
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/address/billingAddress.html",
            controller: "CartCtrl",
            resolve: {
              cartSummary: ['CartService', function(cartService) {
                return cartService.getCartSummary()
              }]
            }
          }
        }
      }).state('app.payment', {
        url: "/payment",
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/payment/payment.html",
            controller: "CartCtrl",
            resolve: {
              cartSummary: ['CartService', function(cartService) {
                return cartService.getCartSummary();
              }],
              showBillAddress: ['CartService', '$rootScope', function(cartService, $rootScope) {
                return $rootScope.smryAddr = cartService.smryAddr;
              // as we are getting the address on clicking placeOrder we dont need to call the same function here now
              //  return cartService.savedBillAddress();
              }],
              // getHashKey: ['CartService', '$rootScope', function(cartService, $rootScope) {
              //   return cartService.getHash();
              // }]
            }
          }
        }
      }).state('app.thankyou', {
        url: "/thankyou?orderId",
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/payment/thankyou.html",
            controller: "CartThankyouCtrl",
            resolve: {
              showBillAddress: ['CartService', function(cartService) {
                //return cartService.savedBillAddress();
              }],
              cartItems: ['CartService', function(cartService) {
                return cartService.makeCartCountZero();
              }]
            }
          }
        }
      }).state('app.order-tracker', {
        url: "/order-tracker",
        views: {
          'menuContent': {
            templateUrl: "app-modules/cart/order/order-tracker.html",
            controller: 'orderTrackerCtrl',
            resolve: {
              orderTrackData: ['orderService', function(orderService) {
               // return orderService.getOrderTrack()
              }],
              orderHistoryData: ['orderService', function(orderService) {
              //  return orderService.getOrderHistory()
              }]
            }
          }
        }
      })
      .state('app.design-makeover', {
        url: "/design-makeover",
        views: {
          'menuContent': {
            templateUrl: "app-modules/design_makeover/easySteps/easy-steps.html",
            controller: 'designMakeoverCtrl',
            resolve: {
              mkResult: ['makeOverService', 'UtilityService', function(makeOverService, UtilityService) {
                 if(UtilityService.getDeviceInfo().id != '12233434'){
                       googleGA.gaView('DDS')
                }
// return makeOverService.getUserDesignEntry();
              }]
            }
          }
        }
      }).state('app.design-makeover-tag-your-photo', {
        url: "/design-makeover-tag-your-photo?activeTab=1",
        views: {
          'menuContent': {
            templateUrl: "app-modules/design_makeover/tagYourPhoto/tag-your-photo.html",
            controller: 'designMakeoverCtrl',
            resolve: {
              mkResult: ['makeOverService', function(makeOverService) {
             //   return makeOverService.getUserDesignEntry();
              }]
            }
          }
        }
      }).state('app.design-makeover-details', {
        url: "/design-makeover-details?activeTab=2",
        views: {
          'menuContent': {
            templateUrl: "app-modules/design_makeover/mkDetail/details.html",
            controller: 'designMakeoverCtrl',
            resolve: {
              mkResult: ['makeOverService', function(makeOverService) {
              //  return makeOverService.getUserDesignEntry();
              }]
            }
          }
        }
      }).state('app.design-makeover-styleQuiz', {
        url: "/design-makeover-styleQuiz?activeTab=3",
        views: {
          'menuContent': {
            templateUrl: "app-modules/design_makeover/styleQuiz/styleQuiz.html",
            controller: 'designMakeoverCtrl',
            resolve: {
              mkResult: ['makeOverService', function(makeOverService) {
//return makeOverService.getUserDesignEntry();
              }]
            }
          }
        }
      })
      .state('app.design-makeover-result', {
        url: "/design-makeover-result",
        views: {
          'menuContent': {
            templateUrl: "app-modules/design_makeover/result/result.html",
            controller: 'designMakeoverCtrl',
            resolve: {
              mkResult: ['makeOverService', function(makeOverService) {
                // return makeOverService.getUserDesignEntry();
              }],
              showPopUp:['makeOverService', '$rootScope', function (makeOverService, $rootScope) {
                var visitCount = localStorage.getItem('mkOverResultVisit');

                if(!visitCount){
                    localStorage.setItem('mkOverResultVisit', 1);
                    visitCount = localStorage.getItem('mkOverResultVisit');
                }
                if(!makeOverService.mkOverRcmdLooks){
                      if(visitCount > '1'){
                          $rootScope.mkOverResultVisit = true;
                        }else{
                          visitCount++
                          localStorage.setItem('mkOverResultVisit', visitCount);
                       }
                }
              }]
            }
          }
        }
      }).state('app.design-makeover-quizResult', {
        url: "/design-makeover-quizResult?newUser",
        views: {
          'menuContent': {
            templateUrl: "app-modules/design_makeover/quizResult/quizResult.html",
            controller: 'designMakeoverCtrl',
          }
        }
      })
      .state('app.looks-landing', {
        url: "/looks-landing?term_node_tid_depth&lookName&minorsMajor",
        views: {
          'menuContent': {
            templateUrl: "app-modules/looks/landing/looks-landing.html",
            controller: 'looksLandingCtrl',
            resolve: {
              'landingResults': ['looksService', '$stateParams', function(looksService, $stateParams) {
                // return looksService.getLooksLanding($stateParams);
              }],
              'minorFilters': ['looksService', '$stateParams', function(looksService, $stateParams) {
                // var obj = {};
                // if ($stateParams.minorsMajor) {
                //   obj.parent = $stateParams.minorsMajor;
                // } else {
                //   obj.parent = $stateParams.term_node_tid_depth;
                // }
               // return looksService.getLooksMinors(obj);
              }]
            }
          }
        }
      })
      .state('app.looks-detail', {
        url: "/looks-detail?spaceVal&styleVal&nid",
        views: {
          'menuContent': {
            templateUrl: "app-modules/looks/detail/looks-detail.html",
            controller: 'looksDetailCtrl',
            params: {
              reload: false
            }, //add reload param to a view you want to reload
            resolve: {
              'delay': ['UtilityService', function(UtilityService) {
             //   return UtilityService.delayPageLoad();
              }],
              'looksDetail': ['looksService', '$stateParams', function(looksService, $stateParams) {
             //   return looksService.getLooksDetail($stateParams.nid);
              }]
            }

          }
        }
      })
      .state('app.looks', {
        url: "/looks",
        views: {
          'menuContent': {
            templateUrl: "app-modules/looks/looks.html",
            controller: 'looksCtrl',
            resolve: {
              'majors': ['looksService', function(looksService) {
                // check if the user is loggedIn, if yes - > add the persona from your login API which was hit earlier quizPlayed = true,  NO - > add the key quizPlayed = false
                if(!looksService.looksMajors){
               // return looksService.getLooksMajors();
                }
              }],
              minors: ['looksService', function(looksService) {
             //   return looksService.getLooksMinors();
              }],
              quizCount: ['looksService', 'UtilityService', function(looksService, UtilityService) {
                 if(UtilityService.getDeviceInfo().id != '12233434'){
                      //analytics.trackView('Looks', getUser());
                      googleGA.gaView('Looks');
                  }
            //    return looksService.getUserPersona();
              }]
            }
          }
        }
      })
      .state('app.discover', {
        url: "/discover?styleQuotient?catalogue?playQuizOutside",
        views: {
          'menuContent': {
            templateUrl: "app-modules/discover/discover.html",
            controller: 'discoverCtrl',
            resolve: {
              'getUserPersona': ['discoverService', function(discoverService) {
                return discoverService.getUserPersona();
              }],
              'furniture': ['discoverService', function(discoverService) {
               // return discoverService.getSubItem(discoverUrls.furniture);
              }],
              'furnishing': ['discoverService', function(discoverService) {
                //return discoverService.getSubItem(discoverUrls.furnishing);
              }],
              'decor': ['discoverService', 'UtilityService', function(discoverService, UtilityService) {
                  if(UtilityService.getDeviceInfo().id != '12233434'){
                     // analytics.trackView('Discover');
                      googleGA.gaView('Discover')
                  }
               // return discoverService.getSubItem(discoverUrls.decor);
              }],
              // 'styleQproducts' :  ['discoverService', function(discoverService) {
              //   if(localStorage.getItem("userUid") !== null){
              //     var minorId = localStorage.getItem('personaMinor');
              //     var majorId = localStorage.getItem('personaMajor');
              //     return discoverService.resultList(true, minorId, majorId);
              //   }
              // }],
              'prodCata': ['searchService', function(searchService) {
                // if(localStorage.getItem("userUid") !== null){
                //   return searchService.addCategory();
                // }
              }]
            }
          }
        }
      }).state('app.your-style-quotient', {
        url: "/your-style-quotient?page",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result.html",
            controller: 'searchResultCtrl2',
            resolve: {
              productList :  ['discoverService', '$rootScope', function(discoverService, $rootScope) {
                if(localStorage.getItem("userUid") !== null){
                  var minorId = localStorage.getItem('personaMinor');
                  var majorId = localStorage.getItem('personaMajor');
                  $rootScope.$emit('resetFilters');
                 // return discoverService.resultList(true, minorId, majorId);
                }
              }],
              prodCata: ['searchService', function(searchService) {
                  //  return searchService.addCategory();
              }]
            }
          }
        }
      })
      // field_product_space_tid[]=77&field_persona_tid[]=35&field_persona_tid[]=36"
      .state('app.discover-product', {
        url: "/discover-product?spaceVal",
        views: {
          'menuContent': {
            templateUrl: "app-modules/discover/discoverproduct/discover-product.html",
            controller: 'discoverProductCtrl',
            resolve: {
              'delay': ['UtilityService', function(UtilityService) {
                return UtilityService.delayPageLoad();
              }]
            }
          }
        }
      }).state('app.discover-category-list', {
        url: "/discover-category-list?cateId&cateName",
        views: {
          'menuContent': {
            templateUrl: "app-modules/discover/discoverproduct/discover-category-list.html",
            controller: 'discoverCategoryListCtrl',
            resolve: {
              'delay': ['UtilityService', function(UtilityService) {
                return UtilityService.delayPageLoad();
              }]
            }
          }
        }
      })
      .state('app.filter-color', {
        url: "/filter-color",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/filter-color.html"
          }
        }
      })
      .state('app.search1', {
        url: "/search?combine",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search.html",
            controller: 'searchCtrl',
            resolve: {
              prodCata: ['searchService', function(searchService) {
               //  return searchService.getProdCata();
              }],
              articleCata: ['searchService','UtilityService', function(searchService, UtilityService) {
                if(UtilityService.getDeviceInfo().id != '12233434'){
                      //analytics.trackView('Search', getUser());
                      googleGA.gaView('Search');
                  }
             //   return searchService.getArticleCata();
              }]
            }
          }
        }
      }).state('app.search-result-both', {
        url: "/search-result-both?page&combine&title",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result.html",
            controller: 'searchResultCtrl2',
            resolve: {
              productList: ['searchService', '$stateParams', '$rootScope', function(searchService, $stateParams, $rootScope) {
                  //searchService.searchProd($stateParams.combine, $rootScope.filterUrl);
              }],
              articless: ['searchService', '$stateParams', '$rootScope', function(searchService, $stateParams, $rootScope) {
                  searchService.srchBox = $stateParams.page == "articles" ? 'hide' : 'show';
                 // searchService.searchFeeds($stateParams.combine);
              }],
              prodCata: ['searchService', function(searchService) {
                //  searchService.addCategory();
              }]
            }
          }
        }
      }).state('app.search-result-article', {
        url: "/search-result-article?page&tid&title",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result.html",
            controller: 'searchResultCtrl2',
            resolve: {
              articless: ['searchService', '$stateParams', '$rootScope', function(searchService, $stateParams, $rootScope) {
                searchService.srchBox = $stateParams.page == "articles" ? 'hide' : 'show';
                //return searchService.searchFeeds($stateParams.tid);
              }]             
            }
          }
        }
      })
      .state('app.search-result-category', {
        url: "/search-result-category?page&tid&title&parentId",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result.html",
            controller: 'searchResultCtrl2',
            resolve: {
              productList: ['searchService', '$rootScope', function(searchService, $rootScope) {
                 //searchService.getProdCataItems($rootScope.filterUrl);
              }],
              subCategories: ['searchService', '$stateParams', function(searchService, $stateParams) {
                 //searchService.getSubCategories($stateParams.parentId, $stateParams.tid);
              }]
            }
          }
        }
      })
      .state('app.search-result3', {
        url: "/search-result2?sortVal",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result.html",
            controller: 'searchResultCtrl'
          }
        }
      })
      .state('app.search-result-space', {
        url: "/search-result-space?page&spaceVal&styleVal&title",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result.html",
            controller: 'searchResultCtrl2',
            resolve: {
              productList: ['searchService', '$stateParams', '$rootScope',  function(searchService, $stateParams, $rootScope) {
                   //searchService.prodSpace($stateParams.spaceVal, $rootScope.filterUrl);
              }],
              prodCata: ['searchService', function(searchService) {
                   //searchService.addCategory();
              }]
            }
          }
        }
      }).state('app.search-result-products', {
        url: "/search-result-products?page&tid&spaceVal&styleVal&title",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result-new.html",
            controller: 'resultProductsCtrl2',
            resolve: {
              productList: ['searchService', '$stateParams', function(searchService, $stateParams) {
                return searchService.prodSpace($stateParams.spaceVal);
              }]
            }
          }
        }
      })
      .state('app.discover-result', {
        url: "/discover-result",
        views: {
          'menuContent': {
            templateUrl: "app-modules/discover/discover-result.html",
            controller: 'productCtrl'
          }
        }
      })

    .state('app.product-detail', {
      url: "/product-detail?product_id",
       cache: false,
      views: {
        'menuContent': {
          templateUrl: "app-modules/products/product-detail.html",
          controller: 'productDetailCtrl',
          resolve: {
            'delay': ['UtilityService', function(UtilityService) {
              //return UtilityService.delayPageLoad();
            }],
            'productDetails': ['productService', '$stateParams', function(productService, $stateParams) {
              //  productService.getProductDetails($stateParams.product_id);
            }],
            'productVariants': ['productService', '$stateParams', function(productService, $stateParams) {
               productService.getProdVariants($stateParams.product_id);
            }],
            'relatedProds': ['productService', '$stateParams', function(productService, $stateParams) {
               productService.getRelatedProd($stateParams.product_id);
            }],
            'prodRelatedlooks': ['productService', '$stateParams', function(productService, $stateParams) {
               productService.getProdrealatedLooks($stateParams.product_id);
            }],
            'sizeVariants': ['productService', '$stateParams', function(productService, $stateParams) {
              // productService.getAllSizes($stateParams.product_id);
            }],
            'faq': ['productService', '$stateParams', function(productService, $stateParams) {
               productService.getProdFAQ();
            }],
            'prodInstall': ['productService', '$stateParams', function(productService, $stateParams) {
               productService.getProdInstall();
            }]

          }
        }
      }
    })

    .state('app.model-says', {
      url: "/model-says?nid",
      views: {
        'menuContent': {
          templateUrl: "app-modules/celebrity/model-says.html",
          controller: 'celebrityCtrl',
          resolve: {
            blogPost: ['CelebrityService', '$stateParams', function(celebrityService, $stateParams) {
              //return celebrityService.getDetails($stateParams);
            }],
            // liked: ['CelebrityService', '$stateParams', function(celebrityService, $stateParams) {
            //   return celebrityService.getLiked({
            //     nid: $stateParams.nid,
            //     uid: localStorage.getItem("userUid")
            //   })
            // }]
          }
        }
      }
    })

    .state('app.blog', {
        url: "/blog",
        views: {
          'menuContent': {
            templateUrl: "app-modules/blog/blog.html",
            controller: 'blogCtrl',
            resolve: {
              'feedList': ['DataFactory', 'UtilityService', function(DataFactory, UtilityService) {
                 if(UtilityService.getDeviceInfo().id != '12233434'){
                      //analytics.trackView('Looks', getUser());
                      googleGA.gaView('Blog');
                  }
           //       return DataFactory.GetFeed(true);
              }]
            }

          }
        }
      }).state('app.feed', {
        url: "/feed",
        views: {
          'menuContent': {
            templateUrl: "app-modules/feed/feed.html",
            controller: 'feedNewCtrl',
            resolve: {
              // 'carousel': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getCarousel();
              // }],
              // 'topCata': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getTopCata();
              // }],
              // 'recommProds': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getRecommProd();
              // }],
              // 'recommArticles': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getRecommArticles();
              // }],
              // 'recommLooks': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getRecommLooks();
              // }],
              // 'lookOftheDay': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getLookOftheDay();
              // }],
              // 'looksMajors': ['looksService', function(looksService) {
              //   // return looksService.getLooksMajors();
              // }],
              // 'inspirations': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getInspirations();
              // }],
              // 'boutiqueLooks': ['feedNewService', function(feedNewService) {
              //   // return feedNewService.getBoutiqueLooks();
              // }],
              'spotlights': ['feedNewService', 'UtilityService', function(feedNewService, UtilityService) {
                if(UtilityService.getDeviceInfo().id != '12233434'){
                      //analytics.trackView('Feed', getUser());
                      googleGA.gaView('Feed');
                  }
                // return feedNewService.getSpotLights();
              }],
              'scrollup': ['$rootScope', '$timeout', function ($rootScope, $timeout) {
                    if( typeof $rootScope.scrollMe != 'undefined'){
                      $timeout(function(){
                        $rootScope.scrollMe(0, 0, true);
                      }, 0);
                    }
               }]

              // }],
              // 'feed': ['UtilityService', function(UtilityService) {
              //   return UtilityService.getLikes('feed');
              // }],
              // 'product': ['UtilityService', function(UtilityService) {
              //   return UtilityService.getLikes('product');
              // }],
              // 'look': ['UtilityService', function(UtilityService) {
              //     return UtilityService.getLikes('look');
              //   }]
                /*,
                            'getBuilders': ['builderService', function (builderService) {
                              return builderService.getBuilderMenu();
                            }] */
            }
          }
        }
      })
        .state('app.projects', {
              url: "/project/:id",
              views: {
                  'menuContent': {
                      templateUrl: "app-modules/projects/list/list.html",
                      controller: 'projectListCtrl',
                      resolve: {
                          landing: ['ProjectService', '$stateParams', function (projectService, $stateParams) {
                              projectService.initFlags($stateParams.id);
                              switch ($stateParams.id) {
                                  case "77":
                                      projectService.isLiving = true;
                                      break;
                                  case "78":
                                      projectService.isDine = true;
                                      break;
                                  case "79":
                                      projectService.isBed = true;
                                      break;
                                  case "1":
                                      projectService.isAll = true;
                                      break;
                              }
                          }],
                          products: ['ProjectService', '$stateParams', function (projectService, $stateParams) {
                              return projectService.setProducts($stateParams.id);
                          }],
                          look: ['ProjectService', '$stateParams', function (projectService, $stateParams) {
                              return projectService.setLooks($stateParams.id);
                          }]
                      }
                  }
              }
          })
          .state('app.projects-visualizer', {
              url: "/project-visualizer/:type",
              views: {
                  'menuContent': {
                      templateUrl: "app-modules/projects/visualizer/visualizer.html",
                      controller: 'projectVisualizerCtrl',
                      resolve: {
                          chairs: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              projectService.type = $stateParams.type;
                              projectService.getItems(urlProject.chairs, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.chairs = response.data, "chairs");
                              });
                          }],
                          sofa: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              return projectService.getItems(urlProject.sofa, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.sofa = response.data, "sofa");
                              });
                          }],
                          beds: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              return projectService.getItems(urlProject.beds, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.beds = response.data, "beds");
                              });
                          }],
                          tables: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              return projectService.getItems(urlProject.tables, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.tables = response.data, "tables");
                              });
                          }],
                          decor: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              return projectService.getItems(urlProject.decor, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.decor = response.data, "decor");
                              });
                          }],
                          lighting: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              return projectService.getItems(urlProject.lighting, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.lighting = response.data, "lighting");
                              });
                          }],
                          rugs: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              return projectService.getItems(urlProject.rugs, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.rugs = response.data, "rugs");
                              });
                          }],
                          textiles: ['ProjectService', '$stateParams', 'UtilityService', function (projectService, $stateParams, utilityService) {
                              return projectService.getItems(urlProject.textiles, {
                                  params: {
                                      uid_raw: utilityService.getUserInfo().uid,
                                      field_user_proj_prod_space_tid: typeof $stateParams.type !== "undefined" ? $stateParams.type : ""
                                  }
                              }).then(function (response) {
                                  projectService.trimContent(projectService.textiles = response.data, "textiles");
                              });
                          }]
                      }
                  }
              }
          })
          .state('app.projects-detail', {
              url: "/project-detail/:nid",
              views: {
                  'menuContent': {
                      templateUrl: "app-modules/projects/details/detail.html",
                      controller: 'projectDetailCtrl',
                      resolve: {
                          detail: ['ProjectService', '$stateParams', function (projectService, $stateParams) {
                              return projectService.getDetails(urlProject.detail, {
                                  nid: $stateParams.nid
                              });
                          }]
                      }
                  }
              }
          })
      .state('app.stylepro', {
        url: "/stylepro/:personaMajor",
        views: {
          'menuContent': {
            templateUrl: "app-modules/stylepro/listing/listing.html",
            controller: 'styleproCtrl',
            resolve: {
              list: ['ListingService', function(listingService) {
              //  return listingService.getStyleProList();
              }],
              typeList: ['ListingService', function(listingService) {
                // return listingService.getStyleProTypes();
              }],
              locationList: ['ListingService', function(listingService) {
                // return listingService.getStyleProLocations();
              }],
              styleList: ['ListingService', function(listingService) {
                // return listingService.getStyleProStyles();
              }]
            }
          }
        }
      }).state('app.stylepro-details', {
        url: "/stylepro-details/:id",
        views: {
          'menuContent': {
            templateUrl: "app-modules/stylepro/details/details.html",
            controller: 'styleproDetailsCtrl',
            resolve: {
              list: ['ListingService', '$stateParams', function(listingService, $stateParams) {
                // return listingService.getStyleProDetails($stateParams.id);
              }],
              followStatus: ['ListingService', '$stateParams', function(listingService, $stateParams) {
                // return listingService.getFollowStatus(localStorage.userUid, $stateParams.id);
              }]
            }
          }
        }
      }).state('app.builder', {
        url: "/builder?nid",
        views: {
          'menuContent': {
            templateUrl: "app-modules/builders/builder-project/builder-project.html",
            controller: 'builderProjectCtrl',
            resolve: {
              'proDetails': ['builderService', '$stateParams', function(builderService, $stateParams) {
                //  return builderService.getBuilderProject($stateParams.nid);
              }]
            }

          }
        }
      }).state('app.builder-property', {
        url: "/builder-property?nid",
        views: {
          'menuContent': {
            templateUrl: "app-modules/builders/builder-project/builder-property-details.html",
            controller: 'builderProjectCtrl',
            resolve: {
              'proDetails': ['builderService', '$stateParams', function(builderService, $stateParams) {
               // return builderService.getBuilderProject($stateParams.nid);
              }]
            }
          }
        }
      }).state('app.builder-look', {
        url: "/builder-look?type&style&propertyid",
        views: {
          'menuContent': {
            templateUrl: "app-modules/builders/builder-look/builder-look.html",
            controller: 'builderLookCtrl',
            resolve: {
              'lookList': ['builderService', '$stateParams', function(builderService, $stateParams) {
                //return builderService.getBuilderProjectList($stateParams.type, $stateParams.style, $stateParams.propertyid);
              }],
              'getBuilderSpace': ['builderService', function (builderService){
               //   return builderService.getBuilderSpace();
              }]
            }
          }
        }
      }).state('app.builder-look-details', {
        url: "/builder-look-details?id",
        views: {
          'menuContent': {
            templateUrl: "app-modules/builders/builder-look-detail/builder-look-detail.html",
            controller: 'builderLookDetailCtrl',
            resolve: {
              'builderLookDetail': ['builderService', '$stateParams', function(builderService, $stateParams) {
                // return builderService.getBuilderLookDetail($stateParams.id);
              }]
            }
          }
        }
      })
      .state('app.builder-product-details', {
        url: "/builder-product-details?product_id",
        views: {
          'menuContent': {
            templateUrl: "app-modules/builders/builder-product-detail/builder-product-detail.html",
            controller: 'builderProductDetailCtrl',
            resolve: {
              'builderProductDetail': ['builderService', '$stateParams', function(builderService, $stateParams) {
                return builderService.getBuilderProductDetail($stateParams.product_id);
              }]
            }
          }
        }
      })
      .state('app.builder-thankyou', {
        url: "/builder-thankyou",
        views: {
          'menuContent': {
            templateUrl: "app-modules/builders/builder-look-thankyou/builder-look-thankyou.html",
            controller: 'builderLooksThankYouCtrl'
          }
        }
      })
      .state('app.builder-design-service', {
        url: "/builder-design-service",
        views: {
          'menuContent': {
            templateUrl: "app-modules/builders/builder-look-thankyou/builder-design-services.html",
            controller: 'builderLooksThankYouCtrl'
          }
        }
      })
      .state('app.no-internet', {
        url: "/no-internet",
        views: {
          'menuContent': {
            templateUrl: "app-modules/static/no-internet.html",
            controller: 'noNetCtrl'
          }
        }
      }).state('app.payu', {
        url: "/payu",
        views: {
          'menuContent': {
            templateUrl: "app-modules/static/static.html",
            controller: "StaticCtrl",
            resolve: {
              staticTemplate: ['StaticService', 'CartService', function(staticService, cartService) {
                staticService.template = cartService.payUPage;
                //  staticService.logoimg = "response[0].header_image";
              }]
            }
          }
        }
  }).state('app.payuIframe', {
    url: "/payuIframe",
    views: {
      'menuContent': {
        templateUrl: "app-modules/cart/payment/payuiframe.html",
        controller: "AppCtrl"
      }
    }
  }).state('app.hero-banner-detail', {
        url: "/hero-banner-detail?page&filterBy&title&id",
        views: {
          'menuContent': {
            templateUrl: "app-modules/search/search-result.html",
            controller: 'searchResultCtrl2',
            resolve: {
              productList: ['searchService', '$stateParams', '$rootScope',  function(searchService, $stateParams, $rootScope) {
                // if($stateParams.filterBy == 'space'){
                //   return searchService.prodSpace($stateParams.spaceVal, $rootScope.filterUrl);
                // }else if($stateParams.filterBy == 'category'){
                //   return searchService.getProdCataItems($rootScope.filterUrl);
                // }else if($stateParams.filterBy == 'brand'){
                //   $rootScope.searchResultTitle = $stateParams.title || "Special Offers";
                //   return searchService.getAllProducts($rootScope.filterUrl+'&field_brands_tid[]='+$stateParams.id);
                // }
              }],
              prodCata: ['searchService', '$stateParams', '$rootScope', function(searchService, $stateParams, $rootScope ) {

                // if($stateParams.filterBy == 'space' || $stateParams.filterBy == 'brand'){
                //     return searchService.addCategory();
                //   }else if($stateParams.filterBy == 'category'){
                //       return searchService.getSubCategories($rootScope.filterUrl);
                //   }
              }]
            }
          }
        }
      }).state('app.boutiques', {
        url:"/boutiques",
        views:{
          'menuContent':{
            templateUrl:'app-modules/boutique/boutique.html',
            controller:'boutiqueCtrl',
            resolve:{
              getdata:['boutiqueService', function(boutiqueService){

                 // return boutiqueService.getBoutique()

              }]
            }
          }
        }
      }).state('app.boutique-detail', {
        url: "/boutique/detail?spaceVal&styleVal&nid",
        views: {
          'menuContent': {
            templateUrl: "app-modules/boutique/detail/boutique-detail.html",
            controller: 'boutiqueDetailCtrl',
            resolve:{
              getData:['boutiqueService','$stateParams', function(boutiqueService, $stateParams){
                //return boutiqueService.getBoutiqueDetail($stateParams.nid)
              }]
            }
          }
        }
      }).state('app.offers', {
        url:"/offers",
        views:{
          'menuContent':{
            templateUrl:'app-modules/boutique/boutique.html',
            controller:'offersCtrl',
            // resolve:{
            //   getdata:['offersService', function(offersService){
            //      // return offersService.getOffers()
            //   }]
            // }
          }
        }
      }).state('app.offers-detail', {
        url: "/offers/detail?spaceVal&styleVal&nid",
        views: {
          'menuContent': {
            templateUrl: "app-modules/boutique/detail/boutique-detail.html",
            controller: 'offersDetailCtrl',
            // resolve:{
            //   getData:['offersService','$stateParams', function(offersService, $stateParams){
            //      //return offersService.getOffersDetail($stateParams.nid)
            //   }]
            // }
          }
        }
      });


// $urlRouterProvider.otherwise('/app/feed');
$urlRouterProvider.otherwise('/app/feed');
});
