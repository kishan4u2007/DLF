/**
 *@description:This Function is used to filter path from API
 * */
appModuleController.filter('removePublc', function () {
    return function (uri, options) {
        var url = typeof options !== "undefined" ? LIVE_URL + options + "/" : imgPath;
        return uri !== null && typeof uri !== "undefined" && uri.length ? uri.replace('public://', url) : "";
    };
})
/**
 *@description:This Function is used to send '&' symbol in a string +%26+
 * */
appModuleController.filter('replaceString', function () {
    return function (value) {
        var value = value.replace('&', '+%26+')
        return value;
    };
})


/**
 *@description:This Function is used to remove space
 * */
appModule.filter('removeSpacesThenLowercase', function () {
    return function (text) {
        var str = typeof text !== "undefined" ? text.replace(/\s+/g, '-') : "";
        return str.toLowerCase();
    };
});
/**
 *@description:This Function is used to encode URL
 * */
appModule.filter('encodeMyURL', function () {
    return function (str) {
        return encodeURI(str);
    }
});

appModule.filter('getPrice', function () {
    return function (data) {
        if (data != undefined && typeof data != NaN) {
            return data.replace(/[A-Za-z]+/g, "");
        } else {
            return data
        }
    }
})