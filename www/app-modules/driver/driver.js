// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova'])



.controller('driverCtrl', ['$scope', '$ionicPopup', '$ionicPlatform', '$state', '$http', '$cordovaGeolocation', 'ScoreDataService','$q','$rootScope', function($scope, $ionicPopup, $ionicPlatform, $state, $http, $cordovaGeolocation, ScoreDataService, $q, $rootScope) {
      // var Origin = 'lat + long';
      // var Destination='28.510353, 77.086080';
      // var url = 'https://maps.googleapis.com/maps/api/directions/json?origin=Origin&destination=Destination&key=AIzaSyC8XHs8wXZ0Tz9QZ7c7pUNjaB-KN89GV_o';
      

     /** $scope.scores = [{name:'DLF Mall 1', URL:'https://maps.googleapis.com/maps/api/directions/json?origin=28.5131131,%2077.0886377&destination=28.510353,%2077.086080&key=AIzaSyC8XHs8wXZ0Tz9QZ7c7pUNjaB-KN89GV_o'},
                      {name:'DLF Mall 2', URL:'https://maps.googleapis.com/maps/api/directions/json?origin=28.5131131,%2077.0886377&destination=28.495975, 77.088941&key=AIzaSyC8XHs8wXZ0Tz9QZ7c7pUNjaB-KN89GV_o'},
                      {name:'DLF Mall 3', URL:'https://maps.googleapis.com/maps/api/directions/json?origin=28.5131131,%2077.0886377&destination=28.495975, 76.088941&key=AIzaSyC8XHs8wXZ0Tz9QZ7c7pUNjaB-KN89GV_o'},
                      {name:'DLF Mall 4', URL:'https://maps.googleapis.com/maps/api/directions/json?origin=28.5131131,%2077.0886377&destination=28.495975, 78.088941&key=AIzaSyC8XHs8wXZ0Tz9QZ7c7pUNjaB-KN89GV_o'}];
 **/

 


 $scope.mallids = [
  {'name':'Nelson Mandela Marg, Vasant Kunj II, Vasant Kunj, New Delhi, Delhi 110070, India', 'destination':'28.542852, 77.155753', 'id':"1"}, 
  {'name':'Saket District Centre, District Centre, Sector 6, Pushp Vihar, New Delhi, Delhi 110017, India', 'destination':'28.528451, 77.216756', 'id':"2"}, 
  {'name':'Mall Back Entry Road, Vasant Kunj II, Vasant Kunj, New Delhi, Delhi 110070, India', 'destination':'28.545176, 77.156267', 'id':"3"}, 
  {'name':'Ambience Mall, Gurgaon', 'destination':'28.509345, 77.097585', 'id':"4"}];

 
//$scope.score = $scope.mallids[0];
 $scope.latlongs  = {
  "mall1" :'28.512786199999997, 77.0886377',
  "mall2" :'28.512786199999997, 77.0886377',
  "mall3" :'28.512786199999997, 77.0886377',
  "mall4" :'28.512786199999997, 77.0886377',

 };    
//  $scope.getScoreData = function(score){
    //          ScoreDataService.getScoreData(score).then(function (result) {
    //              $scope.distance = result.data.routes[0].legs[0].distance.text; 
    //              $scope.duration = result.data.routes[0].legs[0].duration.text;        
    //         }, function (result) {
    //           alert("Error: No data returned");
    //     });
    // }; 

// Getting lat and long from google API
  var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation
     .getCurrentPosition(posOptions)
     .then(function (position) {
      $scope.lat  = position.coords.latitude
      $scope.long = position.coords.longitude
      $rootScope.startPoint = $scope.lat + ',' + $scope.long;
      console.log($rootScope.startPoint)
     }, function(err) {
      // error
    });

     console.log($rootScope.startPoint)


//   $ionicPlatform.ready(function() {
//  //document.addEventListener("deviceready", function () {
//    console.log("now ready");
//    var posOptions = {timeout: 10000, enableHighAccuracy: false};
//      $cordovaGeolocation
//   .getCurrentPosition(posOptions)
//  .then(function (position) {
//  $scope.lat  = position.coords.latitude
//  $scope.long = position.coords.longitude
//  $rootScope.startPoint = $scope.lat + ',' + $scope.long; 
  
//  }, function(err) {
//   console.log("Error occurred!");
//  });
//  var watchOptions = {
//     timeout : 3000,
//     enableHighAccuracy: false // may cause errors if true
//   };
//   watch.then(
//     null,
//     function(err) {
//       // error
//     },
//     function(position) {
//       var lat  = position.coords.latitude
//       var long = position.coords.longitude
//       $rootScope.startPoint = $scope.lat + ',' + $scope.long
//        alert("You want to share your location" +  " " + $rootScope.startPoint);
//   });

//     watch.clearWatch();
//   // OR
//   $cordovaGeolocation.clearWatch(watch)
//     .then(function(result) {
//       // success
//       }, function (error) {
//       // error
//     });


//  // }, false);
// });
$scope.onPageLoad = function() {
  setTimeout();
}

setTimeout(function(){
  var directionsService = new google.maps.DirectionsService;
  var distances = []; 
  var distance; 
  var i,j=0;
  for ( i = 0; i < $scope.mallids.length; i++) {    
    directionsService.route({
    origin: $rootScope.startPoint,
    destination: $scope.mallids[i].destination,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {  
       distances[j] =  response.routes[0].legs[0].distance.value;       
       //distances[j] =  response.routes[0].legs[0]; 
       console.log(response.routes[0].legs[0].distance.text)
      j++;     
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}
//@TODO loop through distances to find minimum distance
console.log(distances)
var min = $scope.findIndexOfGreatest(distances);
console.log("Minumum distances Mall" + ' ' +  min);
$scope.score = $scope.mallids[min];
 $scope.getDistance($scope.score);
// $scope.score.push($scope.score2);
}, 10)


$scope.findIndexOfGreatest = function(array) {
  var smallest=0;
  var indexOfSmallest;
  for (var i = 0; i < 4; i++) {
    if (!smallest || array[i] <= smallest) {
      smallest = array[i];
      indexOfSmallest = i;
    }
  }
  return indexOfSmallest;
}

 $scope.getDistance = function(score){
  console.log(score)
  // console.log(score);
  var deferred = $q.defer();
  var promise = deferred.promise;
  var directionsService = new google.maps.DirectionsService;
  var distance;
  directionsService.route({
    origin: '28.5127106, 77.0883024',
    destination: score.destination,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {   
       deferred.resolve(distance);     
      //console.log(response)      
      distance =  response.routes[0].legs[0];  
      document.getElementById("myDistance").innerHTML =  distance.distance.text; 
      document.getElementById("duration").innerHTML =  distance.duration.text;
      document.getElementById("end").innerHTML =  distance.end_address;        
      $scope.myDistance = distance.distance.text; 
      $scope.duration = distance.duration.text;
      $scope.endAddress = distance.end_address;

      console.log($scope.endAddress)

    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}


      
//using http post as we are passing password.
      // $http({    
      //       url: url, 
      //       method :'GET'
      // })
      // .then(function (res){  //if a response is recieved from the server.      
      //   $scope.distance = res.data.routes[0].legs[0].distance.text; //contains Register Result 
      //   $scope.duration = res.data.routes[0].legs[0].duration.text;
      //   console.log(res.data.routes[0].legs[0].distance.text);
      //   $scope.mall = [
      //   {name : "DLF Mall 1", distance : $scope.distance, duration: $scope.duration},
      //   {name : "DLF Mall 2", color : "white"},
      //   {name : "DLF Mall 3", color : "black"}
      //   ];
      //   $scope.selectedMall = $scope.mall[0]; 
      //   $scope.MallContent = true;
      // }); 


}])

.factory('ScoreDataService', ['$http','$q',  function($http) {    
       var factory = {
            getScoreData: function (score) {  
                console.log(score);
                var data = $http({method: 'GET', url: score.URL});
                return data;
            }
       }       
        return factory;
}]);



