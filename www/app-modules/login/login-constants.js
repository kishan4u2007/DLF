
var login_urls = {
  user_persona: "user_quiz_result/get_persona/persona_result",
  user_login: "login_out/user/login",
  fbLogin: "fb_user/fboauth/connect",
  register: "login_out/user/register",
  resetPass: "user-info/new_password",
  resetPass2: "user-info/user/request_new_password.json",
  //user_logout: "applogout/user/logout"
  user_logout: "login_out/user/logout",
  getSearchHistory: "usercart/get-user-search/", // userID

};
