// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova','ngMap'])

.run(function($ionicPlatform, $rootScope) {
   $rootScope.logLatLng = function(e) {
          console.log('loc', e.latLng);
  }

   $rootScope.wayPoints = [
          //  {location: {lat:28.5131131, lng: 77.088377}, stopover: true},
          // {location: {lat:28.510353, lng: 77.080080}, stopover: true},
        ];

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {    
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
      $httpProvider.defaults.useXDomain = true;
      $httpProvider.defaults.withCredentials = false;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];

   $stateProvider
    .state('location', {
      url: '/',
      templateUrl: 'app-modules/location/location.html',
      controller: 'locationCtrl'
    })
    .state('driver', {
      url: '/driver',
      templateUrl: 'app-modules/driver/driver.html',
      controller: 'driverCtrl'
    })

    $urlRouterProvider.otherwise("/");           
 })

.controller('locationCtrl', ['$scope', '$ionicPopup', '$ionicPlatform', '$state', '$http', '$cordovaGeolocation', '$q','$rootScope','Events','$cordovaCalendar','$timeout', function($scope, $ionicPopup, $ionicPlatform, $state, $http, $cordovaGeolocation,  $q, $rootScope, Events, $cordovaCalendar, $timeout) {

 $scope.showMap = false;

 $scope.showMyLocation = localStorage.getItem("currentPoint");

  // $scope.$on('$ionicView.afterEnter', function(){
  //   setTimeout(function(){
  //     document.getElementById("custom-overlay").style.display = "none";      
  //   }, 4000);
  // }); 




 $scope.mallids = [
  {'name':'Nelson Mandela Marg, Vasant Kunj II, Vasant Kunj, New Delhi, Delhi 110070, India', 'destination':'28.542852, 77.155753', 'id':"1"}, 
  {'name':'Saket District Centre, District Centre, Sector 6, Pushp Vihar, New Delhi, Delhi 110017, India', 'destination':'28.528451, 77.216756', 'id':"2"}, 
  {'name':'Mall Back Entry Road, Vasant Kunj II, Vasant Kunj, New Delhi, Delhi 110070, India', 'destination':'28.545176, 77.156267', 'id':"3"}, 
  {'name':'Ambience Mall, Gurgaon', 'destination':'28.509345, 77.097585', 'id':"4"}];

 
//$scope.score = $scope.mallids[0];
 $scope.latlongs  = {
  "mall1" :'28.512786199999997, 77.0886377',
  "mall2" :'28.512786199999997, 77.0886377',
  "mall3" :'28.512786199999997, 77.0886377',
  "mall4" :'28.512786199999997, 77.0886377',

 };    

  // $ionicPlatform.ready(function() {
document.addEventListener("deviceready", function () {
   $scope.btnCallFun = function() {
      window.plugins.CallNumber.callNumber(function() { alert("calling"); }, function(e){}, "9149257384");
   }

   $scope.otherCalling = function() { 
      window.location.href="tel:9149257384";
   }

   requestWritePermission();
   hasReadWritePermission();
   requestReadWritePermission();
});


$ionicPlatform.ready(function() {
    Events.get().then(function(events) {
      console.log("events", JSON.stringify(events));  
      $scope.events = events;
    });
  });


 $scope.addEvent = function(event,idx) {
    console.log("add ",event);
    
    Events.add(event).then(function(result) {
      console.log("done adding event, result is "+ result);
      if(result === 1) {
        //update the event
        $timeout(function() {
          $scope.events[idx].status = true;
          $scope.$apply();
        });
      } else {
        //For now... maybe just tell the user it didn't work?
      }
    });

    
  };
  

// $scope.showMyCurrentLocation = function() {
//   var posOptions = {timeout: 10000, enableHighAccuracy: false};
//      $cordovaGeolocation
//   .getCurrentPosition(posOptions)
//  .then(function (position) {
//      $scope.lat  = position.coords.latitude
//      $scope.long = position.coords.longitude
//      // $scope.currentPoint = $scope.lat + ',' + $scope.long;
//      localStorage.setItem("currentPoint", $scope.lat + ',' + $scope.long );
//   console.log($scope.currentPoint );   
//  }, function(err) {
//   console.log("Error occurred!");
//  });

// }


//console.log($scope.showMyLocation);

// $scope.showMyCurrentLocation();


// $scope.onPageLoad = function() {
//   // setTimeout();

//   setTimeout(function(){
//   var directionsService = new google.maps.DirectionsService;
//   var distances = []; 
//   var distance; 
//   var i,j=0;
//   for ( i = 0; i < $scope.mallids.length; i++) {    
//     directionsService.route({
//     origin: $scope.showMyLocation,
//     destination: $scope.mallids[i].destination,
//     travelMode: 'DRIVING'
//   }, function(response, status) {
//     if (status === 'OK') {  
//        distances[j] =  response.routes[0].legs[0].distance.value;       
//        //distances[j] =  response.routes[0].legs[0]; 
//        console.log(response.routes[0].legs[0].distance.text)
//       j++;     
//     } else {
//       window.alert('Directions request failed due to ' + status);
//     }
//   });
// }
// //@TODO loop through distances to find minimum distance
// console.log(distances)
// var min = $scope.findIndexOfGreatest(distances);
// console.log("Minumum distances Mall" + ' ' +  min);
// $scope.score = $scope.mallids[min];
//  $scope.getDistance($scope.score);
// // $scope.score.push($scope.score2);
// }, 10)

// }


// $scope.onPageLoad();



// $scope.showDirection = function() { 
//       $scope.showMap =  true;
//    }


// $scope.findIndexOfGreatest = function(array) {
//   var smallest=0;
//   var indexOfSmallest;
//   for (var i = 0; i < 4; i++) {
//     if (!smallest || array[i] <= smallest) {
//       smallest = array[i];
//       indexOfSmallest = i;
//     }
//   }
//   return indexOfSmallest;
// }

// $scope.getDistance = function(score){
//   console.log(score)
//   // console.log(score);
//   var deferred = $q.defer();
//   var promise = deferred.promise;
//   var directionsService = new google.maps.DirectionsService;
//   var distance;
//   directionsService.route({
//     origin: $scope.showMyLocation,
//     destination: score.destination,
//     travelMode: 'DRIVING'
//   }, function(response, status) {
//     if (status === 'OK') {   
//        deferred.resolve(distance);     
//       //console.log(response)      
//       distance =  response.routes[0].legs[0];  
//       document.getElementById("myDistance").innerHTML =  distance.distance.text; 
//       document.getElementById("duration").innerHTML =  distance.duration.text;
//       document.getElementById("end").innerHTML =  distance.end_address;        
//       $scope.myDistance = distance.distance.text; 
//       $scope.duration = distance.duration.text;
//       $scope.endAddress = distance.end_address;

//       console.log($scope.endAddress)

//     } else {
//       window.alert('Directions request failed due to ' + status);
//     }
//   });
// }

}])



.factory('Events', ['$q', '$cordovaCalendar',  function($q, $cordovaCalendar) { 


    //kind of a hack
  var incrementDate = function (date, amount) {
    var tmpDate = new Date(date);
    tmpDate.setDate(tmpDate.getDate() + amount);
    tmpDate.setHours(13);
    tmpDate.setMinutes(0);
    tmpDate.setSeconds(0);
    tmpDate.setMilliseconds(0);
    return tmpDate;
  };
  
  var incrementHour = function(date, amount) {
    var tmpDate = new Date(date);
    tmpDate.setHours(tmpDate.getHours() + amount);
    return tmpDate;
  };
  
  //create fake events, but make it dynamic so they are in the next week
  var fakeEvents = [];
  fakeEvents.push(
    {
      "title":"Meetup on Ionic",
      "description":"We'll talk about beer, not Ionic.",
      "date":new Date()
    } 
  );
  fakeEvents.push(
    {
      "title":"Meetup on Beer",
      "description":"We'll talk about Ionic, not Beer.",
      "date":new Date()
    } 
  );
   fakeEvents.push(
    {
      "title":"Meetup on Angular",
      "description":"We'll talk about Angular, not Beer.",
      "date":new Date()
    } 
  );

 
  
  var getEvents = function() {
      var deferred = $q.defer();    
      /*
      Logic is:
      For each, see if it exists an event.
      */
      var promises = [];
      fakeEvents.forEach(function(ev) {
        //add enddate as 1 hour plus
        ev.enddate = new Date('12-20-2017');
        console.log($cordovaCalendar)
        console.log('try to find '+JSON.stringify(ev));
        promises.push(window.plugins.calendar.findEvent({
          title:ev.title,
          startDate:ev.date
        }));
      });
      
      $q.all(promises).then(function(results) {
        console.log("in the all done"); 
        //should be the same len as events
        for(var i=0;i<results.length;i++) {
          fakeEvents[i].status = results[i].length === 1;
        }
        deferred.resolve(fakeEvents);
      });
      
      return deferred.promise;
  }
  
  var addEvent = function(event) {
    var deferred = $q.defer();

    window.plugins.calendar.createEvent({
      title: event.title,
      notes: event.description,
      startDate: event.date,
      endDate:event.enddate
    }).then(function (result) {
      console.log('success');console.dir(result);
      deferred.resolve(1);
    }, function (err) {
      console.log('error');console.dir(err);
      deferred.resolve(0);
    });     
    return deferred.promise;
  }
  
  return {
    get:getEvents,
    add:addEvent
  };

}]);



