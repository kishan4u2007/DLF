'use strict';

var title1 = 'My Event Title 1';
var loc1 = 'My Event Location 1';
var notes1 = 'My interesting Event notes 1.';
var startDate1 = new Date();
var endDate1 = new Date('12-20-2017');
var calendarName = 'MyCal';


var title2 = 'My Event Title 2';
var loc2 = 'My Event Location 2';
var notes2 = 'My interesting Event notes 2.';
var startDate2 = new Date();
var endDate2 = new Date('12-20-2017');
var calendarName = 'MyCal';



var title3 = 'My Event Title 3';
var loc3 = 'My Event Location 3';
var notes3 = 'My interesting Event notes 3.';
var startDate3 = new Date();
var endDate3 = new Date('12-20-2017');
var calendarName = 'MyCal';



var options = {
  url: 'https://github.com/EddyVerbruggen/Calendar-PhoneGap-Plugin',
  calendarName: calendarName, // iOS specific
  calendarId: 1 // Android specific
};



// clean up the dates a bit
// startDate.setMinutes(0);
// endDate.setMinutes(0);
// startDate.setSeconds(0);
// endDate.setSeconds(0);

// add a few hours to the dates, JS will automatically update the date (+1 day) if necessary
// startDate.setHours(startDate.getHours() + 2);
// endDate.setHours(endDate.getHours() + 3);

function onSuccess(msg) {
  alert('Calendar success: ' + JSON.stringify(msg));
}

function onError(msg) {
  alert('Calendar error: ' + JSON.stringify(msg));
}

function hasReadPermission() {
  window.plugins.calendar.hasReadPermission(onSuccess);
}

function requestReadPermission() {
  window.plugins.calendar.requestReadPermission(onSuccess);
}

function hasWritePermission() {
  window.plugins.calendar.hasWritePermission(onSuccess);
}

function requestWritePermission() {
  window.plugins.calendar.requestWritePermission(onSuccess);
}

function hasReadWritePermission() {
  window.plugins.calendar.hasReadWritePermission(onSuccess);
}

function requestReadWritePermission() {
  window.plugins.calendar.requestReadWritePermission(onSuccess);
}

function openCalendar() {
  // today + 3 days
  var d = new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000);
  window.plugins.calendar.openCalendar(d, onSuccess, onError);
}

function listCalendars() {
  window.plugins.calendar.listCalendars(onSuccess, onError);
}

function createCalendar() {
  window.plugins.calendar.createCalendar(calendarName, onSuccess, onError);
}

function deleteEvent() {
  window.plugins.calendar.deleteEvent(title, loc, notes, startDate, endDate, onSuccess, onError);
}

function createCalendarEventInteractively1() {
  window.plugins.calendar.createEventInteractively(title1, loc1, notes1, startDate1, endDate1, onSuccess, onError);
}

function createCalendarEventInteractively2() {
  window.plugins.calendar.createEventInteractively(title2, loc2, notes2, startDate2, endDate2, onSuccess, onError);
}

function createCalendarEventInteractively3() {
  window.plugins.calendar.createEventInteractively(title3, loc3, notes3, startDate3, endDate3, onSuccess, onError);
}

function createCalendarEventInteractively() {
  window.plugins.calendar.createEventInteractively(title, loc, notes, startDate, endDate, onSuccess, onError);
}

function createCalendarEventInteractivelyWithOptions() {
  window.plugins.calendar.createEventInteractivelyWithOptions(title, loc, notes, startDate, endDate, options, onSuccess, onError);
}

function createCalendarEventWithOptions() {
  window.plugins.calendar.createEventWithOptions(title, loc, notes, startDate, endDate, options, onSuccess, onError)
}

function findEventWithFilter() {
  window.plugins.calendar.findEvent(title, loc, notes, startDate, endDate, onSuccess, onError);
}

function findEventNoFilter() {
  window.plugins.calendar.findEvent(null, null, null, startDate, endDate, onSuccess, onError);
}

window.onerror = function(msg, file, line) {
  alert(msg + '; ' + file + '; ' + line);
};
